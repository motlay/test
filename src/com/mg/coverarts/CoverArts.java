package com.mg.coverarts;

import java.util.HashMap;
import java.util.Map;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

public class CoverArts {
	  private static CoverArts instance = null;
		private static Map<String , String> albumCoverArt = null ;
		private static Map<String , String> artistCoverArt = null ;
		private static Map<String , String> genreCoverArt = null ;
		private static final int ALBUMS_ART = 1;
		private static final int ARTISTS_ART = 0;
		private static final int GENRE_ART = 4;
		private ContentResolver resolver;
	   protected CoverArts() {
	      
	   }
	   public static CoverArts getInstance() {
	      if(instance == null) {
	         instance = new CoverArts();
	      }
	      return instance;
	   }
	   
	   public void loadCoverArts(ContentResolver contentResolver){
		   if(albumCoverArt == null || artistCoverArt == null || genreCoverArt == null){
			   resolver = contentResolver;
			   loadAlbumArts();
			   loadArtistsArts();
			   loadGenreArts();
		   }
	   }
	   
	   
	   public Map<String , String> getCoverArts(int key){
		   Map<String,String> arts = null;
		   switch (key){
		   case ALBUMS_ART : 
			   arts =  albumCoverArt;
			   break;
		   case ARTISTS_ART :
			   arts = artistCoverArt;
			   break;
		   case GENRE_ART:
			   arts = genreCoverArt;
		   default : 
			   break;
		   }
		   return arts;
	   }
	   
	   private void loadAlbumArts() {
			albumCoverArt = new HashMap<String, String>();
			Cursor cursor = resolver.query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,null, null, null, null);
			int length = cursor.getCount();
			for(int i = 1 ; i<length;i++){
				cursor.moveToPosition(i);
				long id = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums._ID));
				String albumArt = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART));
				if(albumArt != null)
					albumCoverArt.put(String.valueOf(id), "file://" +albumArt);

			}

		}
		private void loadArtistsArts() {
			artistCoverArt = new HashMap<String, String>();
			Cursor second = resolver.query(MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI,null, null, null, null);
			int cursorLength = second.getCount();
			for(int j = 0 ; j<cursorLength;j++){
				second.moveToPosition(j);
				long artistId = second.getLong(second.getColumnIndexOrThrow(MediaStore.Audio.Artists._ID));
				String artist = second.getString(second.getColumnIndexOrThrow(MediaStore.Audio.Artists.ARTIST));
				final String[] ccols = new String[] { MediaStore.Audio.Media._ID };
					if(artist != null)
							if(artist.contains("'")){
								artist = artist.replace("'", "''");
							}
				String where = MediaStore.Audio.Albums.ARTIST + "= '" + artist + "'" ;
				Cursor cursor = resolver.query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
						null, where, null, null);
				int length = cursor.getCount();
				for(int i = 0 ; i<length;i++){
					cursor.moveToPosition(i);
					String id = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM));
					String albumArt = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART));
					String art = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ARTIST));
					if(artist.equalsIgnoreCase(art)){
						if(albumArt != null){
							artistCoverArt.put(String.valueOf(artistId), "file://"+albumArt);
							break;
						}
					}

				}
				cursor.close();
			}

			second.close();

		}
		private void loadGenreArts() {
			genreCoverArt = new HashMap<String, String>();
			Cursor second = resolver.query(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI,null, null, null, null);
			int cursorLength = second.getCount();
			for(int j = 0 ; j<cursorLength;j++){
				second.moveToPosition(j);
				long genreId = second.getLong(second.getColumnIndexOrThrow(MediaStore.Audio.Genres._ID));
				final String[] ccols = new String[] { MediaStore.Audio.Media._ID };
				String where = MediaStore.Audio.Albums.ARTIST + "= '" + genreId + "'" ;
				Uri uri = MediaStore.Audio.Albums.getContentUri("external");
				Cursor cursor = null;
				String selection = "album_info._id IN "
						+ "(SELECT (audio_meta.album_id) album_id FROM audio_meta, audio_genres_map "
						+ "WHERE audio_genres_map.audio_id=audio_meta._id AND audio_genres_map.genre_id=?)";
				String[] selectionArgs = new String[] { String.valueOf(genreId) };
				cursor = resolver.query(uri, null, selection,
						selectionArgs, null);
				int length = cursor.getCount();
				for(int i = 0 ; i<length;i++){
					cursor.moveToPosition(i);
					String id = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM));
					String albumArt = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART));
					if(albumArt != null)
						genreCoverArt.put(String.valueOf(genreId),"file://"+ albumArt);
				}
				cursor.close();
			}
			second.close();
		}
}
