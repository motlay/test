package com.mg.ds.sqllite;

import com.mg.util.StringBuilderUtil;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DsSQLLite {

	public static final char COMMA = ',';
	public static final char SPACE = ' ';
	public static final char STAR = '*';
	public static final String INSERT_INTO = "INSERT INTO";
	public static final String VALUES = "VALUES";
	public static final String SELECT = "SELECT";
	public static final String FROM = "FROM";
	public static final String LIMIT = "LIMIT";
	
	public static Cursor get(SQLiteDatabase db, String table, String... fields) {
		StringBuilderUtil qb = new StringBuilderUtil(64);
		qb.
			append(SELECT).append(SPACE);
		if(fields != null)
			qb.append(COMMA, fields).append(SPACE);
		else
			qb.append(STAR).append(SPACE);
		qb.append(FROM).append(SPACE).
		append(table);
		Cursor cursor =  db.rawQuery(qb.toString(), null);
		return cursor;
	}
	
	public static Cursor getFieldFromFirstRow(SQLiteDatabase db, String table, String field) {
		StringBuilderUtil qb = new StringBuilderUtil(64);
		qb.
			append(SELECT).append(SPACE).
			append(field).append(SPACE).
			append(FROM).append(SPACE).
			append(table).append(SPACE).
			append(LIMIT).append(1);
		Cursor cursor =  db.rawQuery(qb.toString(), null);
		return cursor;
	}
	
	public static void drop(SQLiteDatabase db, String table){
		db.execSQL("DROP TABLE IF EXISTS " + table);
	}
	
	public static void create(SQLiteDatabase db, String table ){
		throw new RuntimeException("Method not currently supported");
	}
	
	public static void execute(SQLiteDatabase db, String sql) {
		db.execSQL(sql);
	}
	
	public static Cursor  rawQuery(SQLiteDatabase db, String sql) {
		return db.rawQuery(sql, null);
	}
	
	
	public static void insert(SQLiteDatabase database, String table, Object... fields){
		StringBuilderUtil qb = new StringBuilderUtil(80);
		qb.
			append(INSERT_INTO).append(SPACE).
			append(table).append(SPACE);
		if(fields != null){
			qb.
				append(VALUES).append('(').
				append(COMMA,fields).append(')');
		}
		  database.execSQL(qb.toString());
	  }
}
