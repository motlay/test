package com.mg.util;

public class AppConstants {

	public static final String SONG_NAME = "songName";
	public static final String NAME = "name";
	public static final String FILE_NAME = "fileName"; 
	public static final String SONG_PATH = "songPath";
	public static final String SONG_SIZE = "songSize" ;
}
