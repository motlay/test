package com.mg.util;

public class DatabaseConstants {

	  // Database Constants
	  public static final String MTAG = "mtag";
		
	  // Tables
	  public static final String ACCOUNT = "account";
	  public static final String CONTACT = "contact";
	  public static final String CONTACT_ACCOUNTS = "contact_accounts";
	  public static final String DEDICATION = "dedication";
	  public static final String SHARED = "shared";
	  
	  // Common fields for tables 
	  public static final String MTAG_ID = "mtagId";
	  public static final String OS_ID = "osId";
	  public static final String PHONE_NO = "phoneNo";
	  public static final String SEEN = "seen";
	  public static final String FALSE = "false";
	  public static final String TRUE = "true";
	  public static final String DOWNLOADED = "dowloaded";
	  
	  // Fields for table account
	  public static final String JOINED_ON = "joinedOn";
	  public static final String REGISTRATION_SECRET = "regSecret";
	  public static final String VERIFIED = "verified";
	  // Fields for table contact
	  public static final String FIRST_NAME = "firstName";
	  public static final String LAST_NAME = "lastName";
	  
	  // Field for table dedication
	  public static final String  DEDICATION_ID = "dedicationId";
	  public static final String  SENDER_MTAG_ID = "senderMtagId";
	  public static final String  RECIEVER_MTAG_ID = "recieverMtagId";
	  public static final String  FROM_NAME = "fromName";
	  public static final String  WHEN = "whn";
	  public static final String  TRACK_NAME = "trackName";
	  public static final String  TRACK_PATH = "trackPath";
	  
	  
	  public static final String UPLOADED = "uploaded"; 
	  
	  
	  public static final String CREATE_TABLE_SHARED  = "create TABLE shared (toUser text not null, fromUser  text not null, songPath  text not null, songName  text not null,uploaded  text not null ,whn text not null);";
		public static final String CREATE_TABLE_CONTACT = "create table  " 
			      + "contact "
			      + "(" 
			      + "mtagId" + " text not null, " 
			      + "firstName" + " text not null, " 
			      + "phoneNo" + " text not null " 
			      + ");";
}
