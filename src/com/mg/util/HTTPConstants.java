package com.mg.util;

public class HTTPConstants {

	
	
	public static final String ID = "id";
	public static final String MTAG_ID = "mtagId";
	public static final String PHONE_NUMBER = "phoneNo";
	public static final String UNREGISTERED = "unregisterd";
	public static final String TITLE = "title";
	public static final String IMEI = "imei";
	public static final String OS_TYPE = "os_type";
	public static final String OS_VERSION = "os_version";
	public static final String STATUS = "status";
	public static final String JOINED_ON = "joinedOn";
	public static final String ANDROID = "android";
	public static final String NAME = "name";
	public static final String FROM = "from";
	public static final String TO = "to";
	public static final String FILE = "file";
}
