package com.mg.util;


public class StringBuilderUtil {
	private StringBuilder sb;
	
	public StringBuilderUtil(int capacity){
		sb = new StringBuilder(capacity);
	}
	/**
	 * Appends the o.toString()
	 * @param o the object to be appeneded
	 * @return
	 */
	public StringBuilderUtil append(Object o){
		sb.append(o.toString()); 
		return this;
	}
	/**
	 * Appends s to internal buffer by wrapping it in boundary
	 * at start and at end
	 * @param s
	 */
	public void appendEnclosed(Object s,char boundary){
		sb.append(boundary).append(s.toString()).append(boundary);
	}
	/**
	 * append the array of objects to sb. Those objects which are instance of Strings,
	 * are appended surrounded by single quote - '
	 * @param sep The separator between objects.
	 * @param objects The objects whose toString() will be appeneded to sb
	 */
	public StringBuilderUtil append(char sep, Object... objects) {
		
		for(Object o: objects){
			if(o instanceof String)
				appendEnclosed(o,'\'');
			else
				append(o);
		append(sep);
		}
		//Remove the trailing sep
		if(lastChar() ==  sep)
			deleteTralingChar();
		return this;
	}
	
	/**
	 * @return last character of the buffer
	 * @throws RuntimeException If buffer is empty
	 */
	public char lastChar(){
		if(sb.length() > 0)//If non empty buffer return last char
			return sb.charAt(sb.length()-1); 
		else //If buffer is empty
			throw new RuntimeException("Attempt to retrieve last character of an empty buffer!");
	}
	
	/**
	 * Delete last character of the buffer
	 * @throws RuntimeException If buffer is empty
	 */
	public void deleteTralingChar(){
		if(sb.length() > 0)
			sb.deleteCharAt(sb.length()-1);
		else
			throw new RuntimeException("Attempt to delete last character of an empty buffer!");
	}
	
	@Override
	public String toString(){
		return sb.toString();
	}
}
