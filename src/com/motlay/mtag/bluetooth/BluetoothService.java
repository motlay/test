package com.motlay.mtag.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;

public class BluetoothService {

	private Context context;
	public BluetoothAdapter mBluetoothAdapter;
	private static final String UUID = "eed1d9b5-c06c-462a-8eeb-3680ea511002";
	
	public BluetoothService(Context context){
		this.context = context;
		init();
	}

	private void init() {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	}

	public boolean checkForBluetooth(){
		return mBluetoothAdapter != null;
	}
	
	public boolean checkForEnableBluetooth(){
		return mBluetoothAdapter.isEnabled();
	}
	
	
}
