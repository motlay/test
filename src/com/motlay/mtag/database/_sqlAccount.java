package com.motlay.mtag.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.util.Log;

public class _sqlAccount {
	public static final String TABLE_USER = "account";
	  public static final String MTAG_ID = "mtagId";
	  public static final String PHONE_NO = "phoneNo";
	  public static final String JOINED_ON = "joinedOn";
	  public static final String REGISTRATION_SECRET = "regSecret";

	  // Database creation SQL statement
	  private static final String DATABASE_CREATE = "create table " 
	      + TABLE_USER
	      + "(" 
	      + MTAG_ID + " text not null, " 
	      + PHONE_NO + " text not null, " 
	      + JOINED_ON + " text not null, " 
	      + REGISTRATION_SECRET + " text not null" 
	      + ");";
	  
	  
	  public static void onCreate(SQLiteDatabase database) {
	    database.execSQL(DATABASE_CREATE);
	  }
	  public static void insert(SQLiteDatabase database,String mtagId , String number , String joinedOn , String secret){
		  String insert = "INSERT INTO " +TABLE_USER+ " VALUES" + "('" + mtagId + "','" + number + "','"
				  + joinedOn + "','" +secret +"')";
		 
	  }
	 
	  public static Cursor getUser(SQLiteDatabase databse){
		  String sql = "select mtagId , phoneNo ,joinedOn,regSecret from account";
		  Cursor cursor =  databse.rawQuery(sql, null);
		  return cursor;
	  }
	  
	  public static String getMtagId(SQLiteDatabase databse){
		  String sql = "select mtagId from mtagUser";
		  Cursor cursor = databse.rawQuery(sql, null);
		  String mtagId = null;
		  if(cursor != null){
				if (cursor.moveToFirst()) {
						return cursor.getString(0);
				}
		  }
		  return mtagId;
	  }
	  public static void onUpgrade(SQLiteDatabase database, int oldVersion,
	      int newVersion) {
		  //TODO 
	    Log.w(_sqlAccount.class.getName(), "Upgrading database from version "
	        + oldVersion + " to " + newVersion
	        + ", which will destroy all old data");
	    database.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
	    onCreate(database);
	  }
	} 