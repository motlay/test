package com.motlay.mtag.history;

import java.util.Date;

import org.mg.mtag.R;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class HistoryAdapter extends BaseAdapter {

	private Context mContext;
	private Cursor mCursor;
    private Typeface typeface = null;
	public HistoryAdapter(Context context, Cursor cursor) {
		mContext = context;
		mCursor = cursor;
		typeface = Typeface.createFromAsset(context.getAssets(), "Roboto/Roboto-Light.ttf");
	}

	@Override
	public int getCount() {
		if (mCursor == null)
			return 0;
		return mCursor.getCount();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = convertView != null ? convertView : inflater.inflate(
				R.layout.list_view_row, parent, false);
		mCursor.moveToPosition(position);
		TextView songName = (TextView) rowView.findViewById(R.id.song_name);
		TextView personName = (TextView) rowView
				.findViewById(R.id.sendTo);
		TextView whenView = (TextView) rowView.findViewById(R.id.when);
		String song = mCursor.getString(3);
		String phoneNo = mCursor.getString(0);
		String whn = mCursor.getString(5);
		Date date = new Date(Long.valueOf(whn));
		whenView.setText(date.toString());
		whenView.setTextColor(Color.GRAY);
		String name = getNameFromNumber(phoneNo);
		songName.setText(song);
		songName.setTextColor(Color.GRAY);
		songName.setTypeface(typeface);
		personName.setText(name);
		personName.setTypeface(typeface);
		return rowView;
	}

	private String getNameFromNumber(String phoneNo) {
		String name = null;
		phoneNo = phoneNo.substring(1);
		name = getNameFromNormalizedNum(phoneNo);
		if(name == null){
			phoneNo = "0" + phoneNo.substring(2);
			name = getNameFromNormalizedNum(phoneNo);
			if(name == null){
				phoneNo = phoneNo.substring(1);
				name = getNameFromNormalizedNum(phoneNo);
				if(name == null){
					name = phoneNo;
				}
			}
		}
		return name;
	}
	private String getNameFromNormalizedNum(String phoneNo){
		Uri contactUri = Uri.withAppendedPath(
				PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNo));
		Cursor fromCursor = mContext.getContentResolver().query(contactUri, new String[]{PhoneLookup.DISPLAY_NAME}, null, null, null);
		String fromName  =  null ; 
		if(fromCursor != null){
			if(fromCursor.moveToFirst()){
				fromName = fromCursor.getString(0);
			}
		}
		fromCursor.close();
		return fromName;
	}

}
