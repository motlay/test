package com.motlay.mtag.history;

import org.mg.mtag.R;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;
import com.motlay.mtag.server.MtagUtils;

public class SongRecieved extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_song_recieved);
		Cursor cursor = getCursor();
		ListView listView = (ListView) findViewById(R.id.songRecieved);
		if(cursor == null  ){
			TextView emptyView = (TextView) findViewById(R.id.emptySend);
			emptyView.setText(R.string.history_recieved);
			emptyView.setTextColor(Color.GRAY);
			emptyView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto/Roboto-Light.ttf"));
			listView.setEmptyView(emptyView);
		}else{
			listView.setAdapter(new HistoryAdapter(getApplicationContext(), cursor));
		}
	}

	private Cursor getCursor() {
		SQLiteDatabase db = getApplicationContext().openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.OPEN_READWRITE, null);
		Cursor cursor = null ; 
		if(MtagUtils.isTableExists(DatabaseConstants.DEDICATION, db)){
			String query = "Select * from "+DatabaseConstants.DEDICATION ;
			cursor  =  DsSQLLite.rawQuery(db, query);
		}
		return cursor;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		return true;
	}
	protected void changeView() {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); 
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); 
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case  android.R.id.home:
			finish();
			return true;
		default :
			return true;
		}
	}

}
