package com.motlay.mtag.history;

import org.mg.mtag.R;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;
import com.motlay.mtag.server.MtagUtils;

public class SongSended extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_song_dedicated);
		
		ListView listView = (ListView) findViewById(R.id.songSended);
		Cursor cursor = getCursor(); 
		if(cursor == null){
			TextView emptyView = (TextView) findViewById(R.id.emptySend);
			emptyView.setText(R.string.history_sent);
			emptyView.setTextColor(Color.GRAY);
			emptyView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto/Roboto-Light.ttf"));
			emptyView.setVisibility(1);
			listView.setEmptyView(emptyView);
		}else{
			listView.setAdapter(new HistoryAdapter(getApplicationContext(), cursor));
		}


	}

	private Cursor getCursor() {

		SQLiteDatabase db = getApplicationContext().openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.OPEN_READWRITE, null);
		Cursor cursor = null ;
		if(MtagUtils.isTableExists(DatabaseConstants.SHARED, db)){
			String query = "Select * from "+DatabaseConstants.SHARED ;
			cursor  =  DsSQLLite.rawQuery(db, query);
		}
		return cursor;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		ActionBar ab = getActionBar();
		ab.setTitle("");
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		Tab tab = ab.newTab();	
		tab.setText("Song Send");
		
		tab.setTabListener(new TabListener() {
			
			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				
				
			}
			
			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				Intent intent = new Intent(getApplicationContext(), SongSended.class);
				startActivity(intent);
				
			}
			
			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}
		});
		
		Tab tab2 = ab.newTab();
		tab2.setText("Song Recieved");
		tab2.setTabListener(new TabListener() {
			
			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				
			}
			
			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				Intent intent = new Intent(getApplicationContext(), SongRecieved.class);
				startActivity(intent);
			}
			
			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}
		});
		ab.addTab(tab);
		ab.addTab(tab2);
		return true;
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); 
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); 
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case  android.R.id.home:
			finish();
			return true;
		default :
			return true;
		}
	}
}
