package com.motlay.mtag.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.mg.mtag.DedicationScreen;
import org.mg.mtag.PlayDedication;
import org.mg.mtag.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.mg.util.DatabaseConstants;

public class DownloadSong extends AsyncTask<Object, Object, Boolean>{
	private NotificationHandler handler;
	private String songName;
	private String songPath;
	private String trackPath;
	private NotificationManager mNotificationManager;
	private Context context ;
	private int fileLength = 0;
	private String santizedSongName;
	private String dedicationId ;
	private String fromName;
	private DownloadSong download;
	private  final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			handler.cancelNotification();
			download.cancel(true);
			context.unregisterReceiver(mMessageReceiver);
		}
	};
	public DownloadSong(Context applicationContext,NotificationManager notificationManager,String song ,String fromName , String id ){
		songName = song;
		santizedSongName = song.replaceAll(" ", "%20");
		this.songPath = "http://69.164.201.52/mtagSongs/"+santizedSongName+".mp3";
		dedicationId = id;
		this.fromName = fromName;
		context = applicationContext;
		mNotificationManager = notificationManager;
		handler = new NotificationHandler(context , null ,songName , false);
		download = this;
		applicationContext.registerReceiver(mMessageReceiver,new IntentFilter("cancelDownload"));
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		handler.sendNotification();
	}


	@Override
	protected Boolean doInBackground(Object... params) {
		boolean isComplete = true ; 
		try{
			URL url = new URL(songPath);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setDoOutput(true);
			urlConnection.connect();
			File storagePath = new File(Environment.getExternalStorageDirectory()+ "/MTAG");
			if(!storagePath.exists())
				storagePath.mkdirs();
			File file = new File(storagePath,songName+".mp3");
			if(file.exists() == false)
				file.createNewFile();
			FileOutputStream fileOutput = new FileOutputStream(file);
			InputStream inputStream = urlConnection.getInputStream();
			int totalSize = urlConnection.getContentLength();
			int downloadedSize = 0;
			byte[] buffer = new byte[1024];
			int bufferLength = 0; //used to store a temporary size of the buffer
			while ( (bufferLength = inputStream.read(buffer)) > 0 )  {
				fileOutput.write(buffer, 0, bufferLength);
				downloadedSize += bufferLength;
				int progress=(int)(downloadedSize*100/totalSize);
				publishProgress(progress);
			}
			fileOutput.close();
			trackPath = file.getPath();
		}catch (Exception e){
			handler.cancelNotification();
			isComplete = false;
			Log.e("Exception", e.getMessage());
		}
		return isComplete;
	}

	private void retryNotification() {
		Intent intent = new Intent(context, DedicationScreen.class);
		intent.putExtra("songName", songName);
		intent.putExtra("name", fromName);
		final Notification notifyDetails = 
				new Notification(R.drawable.icon_new,
						"MTAG",System.currentTimeMillis());
		CharSequence contentTitle = 
				"Song Downloading Fail Retry";
		CharSequence contentText = 
				songName ;
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
		notifyDetails.setLatestEventInfo(context, 
				contentTitle, contentText, pIntent);
		notifyDetails.flags |= Notification.FLAG_AUTO_CANCEL;
		notifyDetails.sound =  RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		mNotificationManager.notify(SIMPLE_NOTFICATION_ID, notifyDetails);
	}

	@Override
	protected void onProgressUpdate(Object... values) {
		super.onProgressUpdate(values);
		handler.updateNotification((Integer) values[0]);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onPostExecute(Boolean result) {
		if(result){
			try {
				rescanSdcard();
			} catch (Exception e) {
				Log.e("Re- scanning Music", e.fillInStackTrace().toString());
			}
			handler.cancelNotification();
			final Notification notifyDetails = 
					new Notification(R.drawable.icon_new,
							"MTAG Sharing!",System.currentTimeMillis());
			CharSequence contentTitle = 
					"MTAG";
			CharSequence contentText = 
					songName +"Click here to view";
			Intent notifyIntent = 
					new Intent(context , PlayDedication.class);
			notifyIntent.putExtra("songPath",trackPath);
			notifyIntent.putExtra("songName",songName);

			PendingIntent intent = PendingIntent.getActivity(context, 0, notifyIntent, 0);
			notifyDetails.setLatestEventInfo(context, 
					contentTitle, contentText, intent);
			notifyDetails.flags |= Notification.FLAG_AUTO_CANCEL;
			notifyDetails.sound =  RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			mNotificationManager.notify(SIMPLE_NOTFICATION_ID, notifyDetails);
			updateDedicationInDatabase();
		}else{
			retryNotification();
		}
		super.onPostExecute(result);
	}
	private void updateDedicationInDatabase() {
		SQLiteDatabase db = context.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.CREATE_IF_NECESSARY , null);
		if(MtagUtils.isTableExists(DatabaseConstants.DEDICATION, db)){
			String updateQuery = "UPDATE "+DatabaseConstants.DEDICATION+" SET "+DatabaseConstants.SEEN+" ='true' WHERE "+DatabaseConstants.DEDICATION_ID + " = '" +dedicationId+ "'";
			db.execSQL(updateQuery);
		}
		db.close();
	}
	private void rescanSdcard() throws Exception{     
		context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, 
				Uri.parse("file://" + Environment.getExternalStorageDirectory())));
		Intent scanIntent = new Intent(Intent.ACTION_MEDIA_MOUNTED, 
				Uri.parse("file://" + Environment.getExternalStorageDirectory()));   
		IntentFilter intentFilter = new IntentFilter(Intent.ACTION_MEDIA_SCANNER_STARTED);
		intentFilter.addDataScheme("file");     
		context.sendBroadcast(scanIntent);    
	}

	private void downloadSong() throws IOException{
		URL url = new URL("http://192.168.209.1:8080/android/file.fromat");
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("GET");
		urlConnection.setDoOutput(true);
		urlConnection.connect();
		File SDCardRoot = new File("/sdcard/"+"MTAG/");
		File file = new File(SDCardRoot,"filename.format");
		FileOutputStream fileOutput = new FileOutputStream(file);
		InputStream inputStream = urlConnection.getInputStream();
		int totalSize = urlConnection.getContentLength();
		int downloadedSize = 0;
		byte[] buffer = new byte[1024];
		int bufferLength = 0; //used to store a temporary size of the buffer
		while ( (bufferLength = inputStream.read(buffer)) > 0 )  {
			fileOutput.write(buffer, 0, bufferLength);
			downloadedSize += bufferLength;
			int progress=(int)(downloadedSize*100/totalSize);
		}
		fileOutput.close();
	}
	private int SIMPLE_NOTFICATION_ID;
}


