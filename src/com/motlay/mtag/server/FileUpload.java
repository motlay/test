package com.motlay.mtag.server;

import java.io.File;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;
import com.mg.util.HTTPConstants;
import com.motlay.mtag.server.CustomMultiPartEntity.ProgressListener;

public class FileUpload extends AsyncTask<Object, Object, Object>{

	private File song ;
	private String fileName ;
	private String from;
	private String to;
	private String firstName;
	private NotificationHandler handler;
	private Context context;
	private int i = 0 ; 
	private boolean isUploaded = false;
	private static boolean uploadComplete = false;
	private String songSizeWithUnit = null ; 
	private long songSize = 0;
	private  String URL = "http://69.164.201.52:9998/song/dedicate";
	private FileUpload fileUpload;
	private  final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			handler.cancelNotification();
			fileUpload.cancel(true);
			context.unregisterReceiver(mMessageReceiver);
		}
	};

	public FileUpload(Context mContext,Map<String,String> info){
		context = mContext;
		mContext.registerReceiver(mMessageReceiver,new IntentFilter( "close notification"));
		song = new File(info.get("fileName"));
		fileName = info.get("songName");
		firstName = info.get("firstName");
		from = info.get("from");
		to = info.get("to");
		fileUpload = this;
	}





	@Override
	protected Object doInBackground(Object... params) {
		songSize = song.length();
		songSizeWithUnit  = MtagUtils.formatFileSize(songSize);
		handler = new NotificationHandler(context , songSizeWithUnit , fileName , true);
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(URL);
		HttpContext httpContext = new BasicHttpContext();
		try
		{

			final float totalSize = Float.valueOf(String.valueOf(songSize));

			CustomMultiPartEntity multipartContent = new CustomMultiPartEntity(new ProgressListener()
			{
				@Override
				public void transferred(long num)
				{
					if(isCancelled()){
						Log.e("MTAG Song Sharing", "Song upload task cancelled");
					}
					publishProgress((int) ((num / (float) totalSize) * 100));
				}
			});


			isUploaded = isAlreadyUploaded();
			if(!isUploaded){
				handler.sendNotification();
				multipartContent.addPart(HTTPConstants.FILE, new FileBody(song));
				
			}
			multipartContent.addPart(HTTPConstants.TITLE, new StringBody(fileName));
			multipartContent.addPart(HTTPConstants.FROM, new StringBody(from));
			multipartContent.addPart(HTTPConstants.TO, new StringBody(to));
			httpPost.addHeader("Content-Encoding", "gzip");
			// Send it
			httpPost.setEntity(multipartContent);
			
			HttpResponse response = httpClient.execute(httpPost, httpContext);
			uploadComplete = true;
			return response;
		}

		catch (Exception e)
		{
			uploadComplete = false;
			Log.e("Exception while uploading song", e.fillInStackTrace().toString());
		}
		return null;
	}

	@Override
	protected void onProgressUpdate(Object... values) {
		if(!isUploaded){
			int value = (Integer) values[0];
			handler.updateNotification((Integer) value);
		}
		super.onProgressUpdate(values);
	}
	@Override
	protected void onPostExecute(Object result) {
		if(!isUploaded){
			handler.cancelNotification();
		}
		if(uploadComplete){
			JSONObject response = MtagUtils.readResponse((HttpResponse) result);
			handler.sendSongSharedNotification(firstName);
			try {
				addInFriendList(response);
			} catch (JSONException e) {
				Log.e("Exception while song uploading adding response in friend list", e.fillInStackTrace().toString());
			}
		}
		else 
			handler.sendSomethingWrongNotification(firstName);

	}

	private void addInFriendList(JSONObject response) throws JSONException {
		if(response != null){
			String mtagId = response.getString("to");
			String phoneNo = to;
			SQLiteDatabase db = context.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.OPEN_READWRITE, null);
			if(!MtagUtils.isTableExists(DatabaseConstants.CONTACT, db)){
				DsSQLLite.execute(db, DATABASE_CREATE);
			}
			phoneNo = MtagUtils.getNormalized(phoneNo).toString();
			String frndQuery =  "Select * from "+DatabaseConstants.CONTACT + " where "+DatabaseConstants.PHONE_NO + " = '" +phoneNo+ "'";
			Cursor cursor = DsSQLLite.rawQuery(db, frndQuery);
			if(cursor != null){
				if(!cursor.moveToFirst()){
					DsSQLLite.insert(db, DatabaseConstants.CONTACT, mtagId,this.firstName,phoneNo);
				}
			}
			updateInUploadQueue(db);
			db.close();
		}
	}

	private void updateInUploadQueue(SQLiteDatabase db) {
		if(MtagUtils.isTableExists(DatabaseConstants.SHARED, db)){
			String updateQuery = "UPDATE "+DatabaseConstants.SHARED+" SET "+DatabaseConstants.UPLOADED+" ='true' WHERE "+"songName" + " = '" +this.fileName+ "'";
			db.execSQL(updateQuery);
		}
	}

	private boolean isAlreadyUploaded(){
		SQLiteDatabase db = context.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.OPEN_READWRITE, null); 
		boolean isUploaded = false; 
		if(MtagUtils.isTableExists(DatabaseConstants.SHARED, db)){
			String sqlQuery =  "Select * from "+DatabaseConstants.SHARED + " WHERE "+"songName" + " = '" +this.fileName+ "' AND  "+DatabaseConstants.UPLOADED + " = 'true'";  ;
			Cursor cursor = DsSQLLite.rawQuery(db, sqlQuery);
			if (cursor != null){
				try {
					while (cursor.moveToFirst()) {  
						if(cursor.getCount() > 1){
							isUploaded = true;
						}
						break;
					}
				} finally {
					cursor.close();
					db.close();
				}
			}
		}
		return isUploaded;
	}

	private static final String DATABASE_CREATE = "create table  " 
			+ "contact "
			+ "(" 
			+ "mtagId" + " text not null, " 
			+ "firstName" + " text not null, " 
			+ "phoneNo" + " text not null " 
			+ ");";
}
