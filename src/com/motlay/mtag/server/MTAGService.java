package com.motlay.mtag.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;

import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;

public class MTAGService extends Service{
	public static final String BROADCAST_ACTION = "com.motlay.mtag.server.NotificationActivity";
	Intent intent;
	String mtagId;
	private static JSONObject responseJson =null;
	Context context;
	private String url = "http://69.164.201.52:9998/notification?mtagId=";
	@Override
	public void onCreate() {
		context = getApplicationContext();
		setAlarm();
	}

	private void startWorking() {
		if(MtagUtils.isInternetAvialable(context)){
			SQLiteDatabase db = context.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.CREATE_IF_NECESSARY , null);
			try{
				String sql = "select * from account";
				Cursor cursor = DsSQLLite.rawQuery(db, sql);
				if(cursor != null){
					cursor.moveToFirst();
					mtagId = cursor.getString(0);
				}
				GetNotification noti = new GetNotification();
				noti.execute("");
				uploadPendingSongs(db);
				db.close();
			}catch(Exception e){
				Log.i("Database in Notification", "User Table not avialable as app is running for first time");
			}
		}else{
			stopSelf();
		}
	}

	private void setAlarm() {
		Calendar cal = Calendar.getInstance();

		Intent intent = new Intent(this, MTAGService.class);
		PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);

		AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		// Start every 300 seconds
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 30*1000*10, pintent); 
		
	}

	private void downloadPendingSongs(SQLiteDatabase db) {
//		if(MtagUtils.isTableExists(DatabaseConstants.DEDICATION, db)){
//			String pendingUploadQuery =  "Select * from "+DatabaseConstants.DEDICATION + " where "+DatabaseConstants.SEEN + " = 'false'";
//			Cursor cur = DsSQLLite.rawQuery(db, pendingUploadQuery);
//			if(cur != null){
//				if(cur.moveToFirst()){
//					String  songName = cur.getString(5);
//					String dedicationId = cur.getString(7);
//					NotificationManager mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
//					DownloadSong song = new DownloadSong(context, mNotificationManager, songName , dedicationId);
//					song.execute("");
//				}
//			}
		//}

	}

	private void uploadPendingSongs(SQLiteDatabase db) {
		if(MtagUtils.isTableExists(DatabaseConstants.SHARED, db)){
			String pendingUploadQuery =  "Select * from "+DatabaseConstants.SHARED + " where "+DatabaseConstants.UPLOADED + " = 'false'";
			Cursor cursor = DsSQLLite.rawQuery(db, pendingUploadQuery);
			if(cursor != null){
				while(!cursor.isAfterLast()){
					boolean moved = cursor.moveToNext();
					if(moved){
						String toUser = cursor.getString(0);
						String fromUser = cursor.getString(1);
						String songPath = cursor.getString(2);
						String songName = cursor.getString(3);
						String name = getNameFromNumber(toUser);
						Map<String,String > info = new HashMap<String, String>();
						info.put("fileName", songPath);
						info.put("songName", songName);
						info.put("from", fromUser);
						info.put("to", toUser);
						info.put("firstName", name);
						FileUpload uploader = new FileUpload(context, info);
						uploader.execute(" ");
						
					}
				}
			}
		}

	}

	@Override
	public void onStart(Intent intent, int startid) {
		startWorking();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mtagId = null;
	}

	@Override
	public boolean stopService(Intent name) {
		mtagId = null;
		return super.stopService(name);
	}

	public class GetNotification extends AsyncTask<Object, Object, Object>{
		@Override
		protected Object doInBackground(Object... param) {
			String notificationURL = url + mtagId;
			URL obj = null;
			try {
				obj = new URL(notificationURL);
			} catch (MalformedURLException e2) {
				Log.e("Notifications Pull", "MalformedURLException while creating url");
			}
			HttpURLConnection con = null;
			try {
				con = (HttpURLConnection) obj.openConnection();
			} catch (IOException e1) {
				Log.e("Notifications Pull", "IO Exception while opening connection");
			}
			try {
				con.setRequestMethod("GET");
			} catch (ProtocolException e) {
				Log.e("Notifications Pull", "Protocol Exception while Setting method");
			}

			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			StringBuffer response = new StringBuffer();
			BufferedReader in;
			try {
				in = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
			} catch (IOException e) {
				Log.e("Notifications Pull", "IO Exception while reading response");
			}
			return response;

		}    

		@Override
		protected void onPostExecute(Object result) {
			JSONObject json =  null;
			if(result != null){
				try {
					json = new JSONObject(result.toString());
				} catch (JSONException e1) {
					Log.e("JSONException", "Error while reading song recieve notification");
				}
				responseJson = json;
				try {
					generateNotification();
				} catch (JSONException e) {
					Log.e("JSONException", "Error while generating song recieve notification");
				}
			}
		}
		@SuppressLint("NewApi")
		private void generateNotification() throws JSONException{
			if(responseJson == null )
				return;
			String status = responseJson.getString("status");
			if(status.isEmpty())
				return;

			int statusType = Integer.parseInt(status);
			if(statusType != HAS_NOTIFICATION)
				return;

			responseJson = saveInDatabase(responseJson);
			MtagUtils.songRecieveNotification(getApplicationContext(), responseJson);
		}
	}
	private static final int HAS_NOTIFICATION = 2;

	public JSONObject saveInDatabase(JSONObject responseJson2) throws JSONException {
		String songName = responseJson.getString("songName");
		String fromName = responseJson.getString("phoneNo");
		String fromId = responseJson.getString("fromMtagId");
		String songPath = responseJson.getString("songPath");
		String toId = responseJson.getString("toMtagId");
		String dedicationId = responseJson.getString("dedicationId");
		String when = responseJson.getString("when");
		String phoneNo = responseJson.getString("phoneNo");
		phoneNo = MtagUtils.getNormalized(phoneNo).toString();
		Uri contactUri = Uri.withAppendedPath(
				PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNo));
		Cursor fromCursor = context.getContentResolver().query(contactUri, new String[]{PhoneLookup.DISPLAY_NAME}, null, null, null);
		if(fromCursor != null){
			if(fromCursor.moveToFirst()){
				fromName = fromCursor.getString(0);
				responseJson2.put("name", fromName);
			}
		}
		SQLiteDatabase db = context.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.CREATE_IF_NECESSARY , null);
		if(!MtagUtils.isTableExists(DatabaseConstants.DEDICATION, db)){
			String DEDICATION_TABLE = "create table " 
					+ DatabaseConstants.DEDICATION
					+ "(" 
					+ DatabaseConstants.SENDER_MTAG_ID + " text not null, "
					+ DatabaseConstants.RECIEVER_MTAG_ID + " text not null, "
					+ DatabaseConstants.SEEN + " text not null, "
					+ DatabaseConstants.FROM_NAME + " text not null , " 
					+ DatabaseConstants.WHEN + " text not null , " 
					+ DatabaseConstants.TRACK_NAME + " text not null ," 
					+ DatabaseConstants.TRACK_PATH + " text not null , " 
					+DatabaseConstants.DEDICATION_ID + " text not null" 
					+ ");";
			DsSQLLite.execute(db, DEDICATION_TABLE);
		}else {

		}
		String dedicationIdQuery =  "Select * from "+DatabaseConstants.DEDICATION + " where "+DatabaseConstants.DEDICATION_ID + " = '" +dedicationId+ "'";
		Cursor cur = DsSQLLite.rawQuery(db, dedicationIdQuery);
		if(cur != null){
			if(!cur.moveToFirst()){
				DsSQLLite.insert(db, DatabaseConstants.DEDICATION,fromId,toId,DatabaseConstants.FALSE,fromName,when,songName,songPath,dedicationId);
				if(!MtagUtils.isTableExists(DatabaseConstants.CONTACT, db)){
					String DATABASE_CREATE = "create table  " 
							+ "contact "
							+ "(" 
							+ "mtagId" + " text not null, " 
							+ "firstName" + " text not null, " 
							+ "phoneNo" + " text not null " 
							+ ");";
					DsSQLLite.execute(db, DATABASE_CREATE);
				}
				String frndQuery =  "Select * from "+DatabaseConstants.CONTACT + " where "+DatabaseConstants.PHONE_NO + " = '" +phoneNo+ "'";
				Cursor cursor = DsSQLLite.rawQuery(db, frndQuery);
				if(cursor != null){
					if(!cursor.moveToFirst()){
						DsSQLLite.insert(db, DatabaseConstants.CONTACT, mtagId,fromName,phoneNo);
					}
				}
			}
		}
		db.close();
		return responseJson2;
	}
	private String getNameFromNumber(String phoneNo) {
		String name = null;
		phoneNo = phoneNo.substring(1);
		name = getNameFromNormalizedNum(phoneNo);
		if(name == null){
			phoneNo = "0" + phoneNo.substring(2);
			name = getNameFromNormalizedNum(phoneNo);
			if(name == null){
				phoneNo = phoneNo.substring(1);
				name = getNameFromNormalizedNum(phoneNo);
				if(name == null){
					name = phoneNo;
				}
			}
		}
		return name;
	}
	private String getNameFromNormalizedNum(String phoneNo){
		Uri contactUri = Uri.withAppendedPath(
				PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNo));
		Cursor fromCursor = context.getContentResolver().query(contactUri, new String[]{PhoneLookup.DISPLAY_NAME}, null, null, null);
		String fromName  =  null ; 
		if(fromCursor != null){
			if(fromCursor.moveToFirst()){
				fromName = fromCursor.getString(0);
			}
		}
		fromCursor.close();
		return fromName;
	}

}
