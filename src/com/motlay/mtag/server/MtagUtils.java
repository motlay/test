package com.motlay.mtag.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.mg.mtag.DedicationScreen;
import org.mg.mtag.R;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class MtagUtils {


	public static JSONObject readResponse(HttpResponse response){
		BufferedReader reader = null;
		JSONObject responseJson = null;
		if(response == null)
			return new JSONObject();
		try {
			reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		StringBuilder builder = new StringBuilder();
		try {
			for (String line = null; (line = reader.readLine()) != null;) {
				builder.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			responseJson = new JSONObject(builder.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJson;
	}

	
	public static void songRecieveNotification(Context context,JSONObject json) throws JSONException{
		NotificationManager mNotificationManager = 
				(NotificationManager)	context.getSystemService(context.NOTIFICATION_SERVICE);
		final Notification notifyDetails = 
				new Notification(R.drawable.icon_new,
						"MTAG Song Dedication!",System.currentTimeMillis());
		CharSequence contentTitle = 
				"MTAG ";
		String songName = json.getString("songName");
		String from = json.getString("name");
		String songPath = json.getString("songPath");
		String dedicationId = json.getString("dedicationId");
		CharSequence contentText = from + " has shared " +
				songName +".mp3  with you Click here to view";
		notifyDetails.sound =  RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Intent notifyIntent = 
				new Intent(context,DedicationScreen.class);
		notifyIntent.putExtra("songName", songName);
		notifyIntent.putExtra("from", from);
		notifyIntent.putExtra("dedicationId",dedicationId);
		notifyIntent.putExtra("songPath", songPath);
		notifyIntent.putExtra("name",json.getString("name"));
		PendingIntent intent = PendingIntent.getActivity(context, 0, notifyIntent, 0);
		notifyDetails.setLatestEventInfo(context, 
				contentTitle, contentText, intent);
		notifyDetails.flags |= Notification.FLAG_AUTO_CANCEL;
		mNotificationManager.notify(Notification.PRIORITY_DEFAULT, notifyDetails);

	}
	public static boolean isInternetAvialable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connectivity != null) 
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();

			if (info != null) 
			{
				for (int i = 0; i < info.length; i++) 
				{
					Log.i("Class", info[i].getState().toString());
					if (info[i].getState() == NetworkInfo.State.CONNECTED) 
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean isTableExists(String tableName, SQLiteDatabase mDatabase) {
		Cursor cursor = mDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
		if(cursor!=null) {
			if(cursor.getCount()>0) {
				cursor.close();
				return true;
			}
			cursor.close();
		}
		return false;
	}

	public static  Long getNormalized(String phoneNumber) {
		Number number = parseNumber(phoneNumber);
		String num = number.toString();
		if(num.startsWith("+91") || num.startsWith("91"))
			return Long.valueOf(num);
		num = "91"+num;
		return Long.valueOf(num);
	}
	public static Number parseNumber(String number) {
		number = number.replaceAll("[^\\.\\d,-]", "");
		DecimalFormat df = new DecimalFormat("#,##,##,###.##");
		Number n;
		try {
			n = df.parse(number);
		} catch (Exception e) {
			return 0.0;
		}
		return n;
	}

	public static String formatFileSize(final long pBytes){
		if(pBytes < KILO){
			return pBytes + BYTES;
		}else if(pBytes < MEGA){
			return (int)(0.5 + (pBytes / (double)KILO)) + KILOBYTES;
		}else if(pBytes < GIGA){
			return (int)(0.5 + (pBytes / (double)MEGA)) + MEGABYTES;
		}else {
			return (int)(0.5 + (pBytes / (double)GIGA)) + GIGABYTES;
		}
	}
	public static  boolean checkServiceStatus(Context context) 
	{ 
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (MTAGService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}


	private static final String BYTES = "Bytes";
	private static final String MEGABYTES = " MB";
	private static final String KILOBYTES = " kB";
	private static final String GIGABYTES = " GB";
	private static final long KILO = 1024;
	private static final long MEGA = KILO * 1024;
	private static final long GIGA = MEGA * 1024;	

}

