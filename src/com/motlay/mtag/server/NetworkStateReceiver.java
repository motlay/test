package com.motlay.mtag.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.NfcAdapter;
import android.util.Log;
import android.widget.Toast;

public class NetworkStateReceiver extends BroadcastReceiver {


	public void onReceive(Context context, Intent intent) {
		if(MtagUtils.isInternetAvialable(context)){
			boolean serviceRunning = MtagUtils.checkServiceStatus(context);
			if(serviceRunning)
				context.stopService(intent);
			{
				ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
				if(connectivityManager != null ){
					Intent service = new Intent(context, MTAGService.class);
					NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
					if (null != activeNetwork) {
						if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
							service.putExtra("type", "wifi");
						else if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
							service.putExtra("type", "mobile");
						context.startService(service);
					} 
				}
			}
		}else{
			boolean serviceRunning = MtagUtils.checkServiceStatus(context);
			if(serviceRunning)
				context.stopService(intent);
		}
		//		if(intent.getExtras()!=null) {
		//			NetworkInfo networkInfo=(NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
		//			Intent service = new Intent(context, NotificationService.class);
		//			if(networkInfo!=null && networkInfo.getState()==NetworkInfo.State.CONNECTED) 
		//				context.startService(service);
		//			else{
		//				Toast.makeText(context, "Network disconnected ", Toast.LENGTH_LONG);
		//				context.stopService(service);
		//			}
		//
		//		}
		//		if(intent.getExtras().getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY,Boolean.FALSE)) {
		//			Log.d("app","There's no network connectivity");
		//		}



	}
}


