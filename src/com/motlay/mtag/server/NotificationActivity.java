package com.motlay.mtag.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.PhoneLookup;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;

public class NotificationActivity extends Activity{

	private String mtagId = null;
	private Context context;
	private static JSONObject responseJson =null;
	private String url = "http://192.168.2.7:9000/notification?mtagId=";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		boolean networkAvialibilty = MtagUtils.isInternetAvialable(this);
		if(networkAvialibilty){
			context = getApplicationContext();
			SQLiteDatabase db = this.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.CREATE_IF_NECESSARY , null);
			try{
				String sql = "select * from account";
				Cursor cursor = DsSQLLite.rawQuery(db, sql);
				if(cursor != null){
					cursor.moveToFirst();
					mtagId = cursor.getString(0);
				}
			}catch(Exception e){
				Log.i("Database in Notification", "User Table not avialable as app is running for first time");
			}
			GetNotification noti = new GetNotification();
			noti.execute("");
			db.close();
			finish();
		}
		finish();
	}

	public class GetNotification extends AsyncTask<Object, Object, Object>{
		@Override
		protected Object doInBackground(Object... param) {
			String notificationURL = url + "52456afe44aeff8897dfa6ec";
			URL obj = null;
			try {
				obj = new URL(notificationURL);
			} catch (MalformedURLException e2) {
				Log.e("Notifications Pull", "MalformedURLException while creating url");
			}
			HttpURLConnection con = null;
			try {
				con = (HttpURLConnection) obj.openConnection();
			} catch (IOException e1) {
				Log.e("Notifications Pull", "IO Exception while opening connection");
			}
			try {
				con.setRequestMethod("GET");
			} catch (ProtocolException e) {
				Log.e("Notifications Pull", "Protocol Exception while Setting method");
			}
	 
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			StringBuffer response = new StringBuffer();
			BufferedReader in;
			try {
				in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
		 
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
			} catch (IOException e) {
				Log.e("Notifications Pull", "IO Exception while reading response");
			}
			
			return response;
			
		}    

		@Override
		protected void onPostExecute(Object result) {
			JSONObject json =  null;
			try {
				json = new JSONObject(result.toString());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			responseJson = json;
			try {
				generateNotification();
			} catch (JSONException e) {
				//TODO
			}
		}
		@SuppressLint("NewApi")
		private void generateNotification() throws JSONException{
			if(responseJson == null )
				return;
			String status = responseJson.getString("status");
			if(status.isEmpty())
				return;
			
			int statusType = Integer.parseInt(status);
			if(statusType != HAS_NOTIFICATION)
				return;
			
			responseJson = saveInDatabase(responseJson);
			MtagUtils.songRecieveNotification(getApplicationContext(), responseJson);
		}
	}
	private static final int HAS_NOTIFICATION = 2;
	
	public JSONObject saveInDatabase(JSONObject responseJson2) throws JSONException {
		String songName = responseJson.getString("songName");
		String fromName = responseJson.getString("phoneNo");
		String fromId = responseJson.getString("fromMtagId");
		String songPath = responseJson.getString("songPath");
		String toId = responseJson.getString("toMtagId");
		String dedicationId = responseJson.getString("dedicationId");
		String when = responseJson.getString("when");
		String phoneNo = responseJson.getString("phoneNo");
		phoneNo = MtagUtils.getNormalized(phoneNo).toString();
			Uri contactUri = Uri.withAppendedPath(
				    PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNo));
		Cursor fromCursor = context.getContentResolver().query(contactUri, new String[]{PhoneLookup.DISPLAY_NAME}, null, null, null);
		if(fromCursor != null){
			if(fromCursor.moveToFirst()){
				fromName = fromCursor.getString(0);
				responseJson2.put("name", fromName);
			}
		}
		SQLiteDatabase db = context.openOrCreateDatabase("mtag",SQLiteDatabase.CREATE_IF_NECESSARY , null);
		if(!MtagUtils.isTableExists(DatabaseConstants.DEDICATION, db)){
			 String DEDICATION_TABLE = "create table " 
				      + DatabaseConstants.DEDICATION
				      + "(" 
				      + DatabaseConstants.SENDER_MTAG_ID + " text not null, " 
				      + DatabaseConstants.RECIEVER_MTAG_ID + " text not null, "
				      + DatabaseConstants.FROM_NAME + " text not null , " 
				      + DatabaseConstants.WHEN + " text not null , " 
				      + DatabaseConstants.TRACK_NAME + " text not null ," 
				      + DatabaseConstants.TRACK_PATH + " text not null , " 
				      +DatabaseConstants.DEDICATION_ID + " text not null" 
				      + ");";
			 DsSQLLite.execute(db, DEDICATION_TABLE);
		}
		
		DsSQLLite.insert(db, DatabaseConstants.DEDICATION,fromId,toId,fromName,when,songName,songPath,dedicationId);
	if(!MtagUtils.isTableExists(DatabaseConstants.CONTACT, db)){
		String DATABASE_CREATE = "create table  " 
			      + "contact "
			      + "(" 
			      + "mtagId" + " text not null, " 
			      + "firstName" + " text not null, " 
			      + "phoneNo" + " text not null " 
			      + ");";
		DsSQLLite.execute(db, DATABASE_CREATE);
	}
	String frndQuery =  "Select * from "+DatabaseConstants.CONTACT + " where "+DatabaseConstants.PHONE_NO + " = '" +phoneNo+ "'";
	Cursor cursor = DsSQLLite.rawQuery(db, frndQuery);
	if(cursor != null){
		if(!cursor.moveToFirst()){
			DsSQLLite.insert(db, DatabaseConstants.CONTACT, mtagId,fromName,phoneNo);
		}
	}
	db.close();
	finish();
	return responseJson2;
}
}

