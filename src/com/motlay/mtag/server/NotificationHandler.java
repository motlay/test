package com.motlay.mtag.server;


import org.mg.mtag.BroadCaster;
import org.mg.mtag.R;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.widget.RemoteViews;

public class NotificationHandler {

	private static final String DOWNLOAD_NOTIFICATION_TAG = "com.cm.wifiscanner.download";
	private static final int DOWNLOAD_NOTIFICATION_ID = 0;
	private static final int UPDATE_INTERVAL = 1000;
	private Context mContext;
	private Notification mDownNotification;
	private RemoteViews mContentView;
	private NotificationManager mNm;
	private String fileSize = null ;
	private String songName = null;
	private long mLastUpdateTime = -1;
	boolean uploading = true;

	
	
	public NotificationHandler(Context context ,String songSize , String name , boolean upload) {
	
		mContext = context;
		mNm = (NotificationManager) mContext
				.getSystemService(Context.NOTIFICATION_SERVICE);
	
		if(name != null){
			songName = name ;
		}
		if(songSize != null){
			fileSize = songSize;
		}
		uploading = upload;
	}

	public void sendNotification() {
		initNotification();
		mNm.notify(DOWNLOAD_NOTIFICATION_TAG, DOWNLOAD_NOTIFICATION_ID,
				mDownNotification);
		mLastUpdateTime = System.currentTimeMillis();
	}

	@SuppressWarnings("deprecation")
	private void initNotification() {
		
		String contextText = "MTAG ";
		mDownNotification = new Notification(R.drawable.mtag_launcher_icon,
				"MTAG Dedication !",System.currentTimeMillis());
		mDownNotification.setLatestEventInfo(mContext, contextText, null, null);
		mDownNotification.flags = Notification.FLAG_ONGOING_EVENT;
//		Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.mtag_launcher_icon);
//		mDownNotification.largeIcon = bitmap;
		
		intitNewContentView();
	}
	private void intitNewContentView(){
		mContentView =  new RemoteViews(mContext.getPackageName(),
				R.layout.songupload_notification);
		
		if(songName != null){
			if(songName.length() >= 20 )
				songName = songName.substring(0, 19);
		}
		if(uploading){
			mContentView.setTextViewText(R.id.uploadSong, "  "+ songName + ".mp3" );
			mContentView.setTextViewText(R.id.progresstext, "0% uploaded");
		}else{
			mContentView.setTextViewText(R.id.uploadSong, "  Downloading  "+ songName + ".mp3 ");
			mContentView.setTextViewText(R.id.progresstext, "0% downloaded");
		}
		
		mContentView.setTextViewText(R.id.songSize, fileSize);
		mContentView.setTextViewText(R.id.progressCancel, "Cancel");
		Intent intent = new Intent(mContext,BroadCaster.class);
		if(uploading)
			intent.setAction("org.kreed.vanilla.FriendSelect.MyPhoneReceiver");
		else 
			intent.setAction("cancelDownload");
		PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		mContentView.setOnClickPendingIntent(R.id.progressCancel, pendingIntent);
		
	
	}
	
	public void sendSongSharedNotification(String toUserName){
		if(songName != null){
			if(songName.length() >= 20 )
				songName = songName.substring(0, 19);
		}
		
		 Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		 Builder mNotificationBuilder  = new Notification.Builder(mContext)
		             .setContentTitle(songName)
		             .setContentText("Shared with  " + toUserName )
		             .setSmallIcon(R.drawable.mtag_launcher_icon)
		             .setSound(soundUri)
		             .setAutoCancel(true);
		 Notification mNotification = mNotificationBuilder.getNotification();      
		 mNm.notify( 0,mNotification);

	}
	
	private void initContentView() {
		mContentView = new RemoteViews(mContext.getPackageName(),
				R.layout.downloand_notification);
		mContentView.setProgressBar(R.id.progressBar, 100, 0, false);
		mContentView.setImageViewResource(R.id.imageView1,
				android.R.drawable.stat_sys_download);
		if(songName != null){
			if(songName.length() >= 20 )
				songName = songName.substring(0, 19);
		}
		if(uploading)
			mContentView.setTextViewText(R.id.uploadingSong, "  "+ songName + ".mp3  ( " +fileSize + " )" );
		else{
			mContentView.setTextViewText(R.id.uploadingSong, "  Downloading  "+ songName + ".mp3 ");
		}
		mContentView.setTextViewText(R.id.progress1Text, "0%");
		mDownNotification.contentView = mContentView;
		
	}

	public void updateNotification(int progress) {
		if (checkUpdateInterval()) {
//			mContentView.setProgressBar(R.id.progressBar, 100,
//					progress, false);
			if(uploading)
			mContentView.setTextViewText(R.id.progresstext, progress
					+ "% uploaded");
			else 
				mContentView.setTextViewText(R.id.progresstext, progress
						+ "% downloaded");
			mDownNotification.contentView = mContentView;
			mNm.notify(DOWNLOAD_NOTIFICATION_TAG, DOWNLOAD_NOTIFICATION_ID,
					mDownNotification);
		}
	}

	private boolean checkUpdateInterval() {
		long currentTime = System.currentTimeMillis();
		if (currentTime - mLastUpdateTime > UPDATE_INTERVAL) {
			mLastUpdateTime = currentTime;
			return true;
		}
		return false;
	}

	public void cancelNotification() {
		if (mDownNotification != null || mNm != null) {
			mNm.cancel(DOWNLOAD_NOTIFICATION_TAG, DOWNLOAD_NOTIFICATION_ID);
		}
	}

	public void sendSomethingWrongNotification(String firstName) {
		 Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		 Builder mNotificationBuilder  = new Notification.Builder(mContext)
		             .setContentTitle(songName)
		             .setContentText("Sharing failed with " +firstName)
		             .setSmallIcon(R.drawable.mtag_launcher_icon)
		             .setSound(soundUri)
		             .setAutoCancel(true);
		 Notification mNotification = mNotificationBuilder.getNotification();      
		 mNm.notify( 0,mNotification);
		
	}
}
