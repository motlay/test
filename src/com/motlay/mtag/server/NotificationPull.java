package com.motlay.mtag.server;

import android.app.IntentService;
import android.content.Intent;

public class NotificationPull extends IntentService{

	public NotificationPull(String name) {
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		startActivity(intent);
	}

}
