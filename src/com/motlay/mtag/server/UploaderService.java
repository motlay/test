
package com.motlay.mtag.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.mg.mtag.R;

import android.app.AlarmManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.util.Log;

public final class UploaderService extends IntentService {
    private static final String TAG = UploaderService.class.getSimpleName();
    public static final long RUN_INTERVAL = AlarmManager.INTERVAL_HOUR;
    private static final String EXTRA_UPLOAD_UNCONDITIONALLY = UploaderService.class.getName()
            + ".extra.UPLOAD_UNCONDITIONALLY";
    private static final int BUF_SIZE = 1024 * 8;
    protected static final int TIMEOUT_IN_MS = 1000 * 4;
    private NotificationHandler mNotificationhandler;
    private boolean mCanUpload;
    private File mFilesDir;
    private URL mUrl;
    private String from;
	private String to;
	private String firstName;
	private String songName ;
    public UploaderService() {
        super("MTAG Uploader Service");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mCanUpload = true;
        mFilesDir = null;
        mUrl = null;

        try {
            final String urlString = getString(R.string.dedication_upload_url);
            if (urlString == null || urlString.equals("")) {
                return;
            }
            mUrl = new URL(urlString);
            mCanUpload = true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (!mCanUpload) {
            return;
        }
        boolean isUploadingUnconditionally = false;
        from = intent.getStringExtra("from");
        to = intent.getStringExtra("to");
        songName = intent.getStringExtra("songName");
        mFilesDir = new File(intent.getStringExtra("fileName"));
        mNotificationhandler.sendNotification();
        doUpload(isUploadingUnconditionally);
    }

    private boolean isExternallyPowered() {
        final Intent intent = registerReceiver(null, new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED));
        final int pluggedState = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        return pluggedState == BatteryManager.BATTERY_PLUGGED_AC
                || pluggedState == BatteryManager.BATTERY_PLUGGED_USB;
    }

    private boolean hasWifiConnection() {
        final ConnectivityManager manager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo wifiInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return wifiInfo.isConnected();
    }

    private void doUpload(final boolean isUploadingUnconditionally) {
        if(uploadFile(mFilesDir)){
        	mNotificationhandler.cancelNotification();
        }
    }

    private boolean uploadFile(File file) {
        Log.d(TAG, "attempting upload of " + file.getAbsolutePath());
        boolean success = false;
        final int contentLength = (int) file.length();
        HttpURLConnection connection = null;
        InputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            connection = (HttpURLConnection) mUrl.openConnection();
            connection.setRequestProperty("title", songName);
            connection.setRequestProperty("from", from);
            connection.setRequestProperty("to", to);
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setFixedLengthStreamingMode(contentLength);
            final OutputStream os = connection.getOutputStream();
            final byte[] buf = new byte[BUF_SIZE];
            int numBytesRead;
            int progress = 0;
            int pro = 0;
            int lastProgressUpdate=0;
            while ((numBytesRead = fileInputStream.read(buf)) != -1) {
            		pro = (numBytesRead + pro );
            		progress =  (pro / contentLength ) * 100;
            	 if(progress%5==0 && progress!=lastProgressUpdate)
            	    {	
            	         mNotificationhandler.updateNotification(progress);
            	         lastProgressUpdate=progress;
            	    }
                os.write(buf, 0, numBytesRead);
            }
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.d(TAG, "upload failed: " + connection.getResponseCode());
                InputStream netInputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(netInputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    Log.d(TAG, "| " + reader.readLine());
                }
                reader.close();
                return success;
            }
            file.delete();
            success = true;
            Log.d(TAG, "upload successful");
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return success;
    }
    
}
