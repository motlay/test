package org.mg.mtag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class AddMessge extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_messge);
		intitViews();
	}

	private void intitViews() {
		Intent intent = getIntent();
		String songPath = intent.getStringExtra("fileName");
		String songName = intent.getStringExtra("songName");
		String name = intent.getStringExtra("name");
		String from = intent.getStringExtra("from");
		String to = intent.getStringExtra("to");
		TextView personName = (TextView) findViewById(R.id.personName);
		personName.setText(name);
		TextView songNameView = (TextView) findViewById(R.id.songName);
		songNameView.setText(songName);
		ImageButton submit = (ImageButton) findViewById(R.id.imageButton1);
		submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sendsong();
				
			}
		});
	}

	protected void sendsong() {
		Intent intent = getIntent();
		String songPath = intent.getStringExtra("fileName");
		String songName = intent.getStringExtra("songName");
		String from = intent.getStringExtra("from");
		String to = intent.getStringExtra("to");
//		FileUpload sendSong = new FileUpload(getApplicationContext(),songPath, songName, from, to);
//		sendSong.execute("");
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_messge, menu);
		return true;
	}

}
