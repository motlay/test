package org.mg.mtag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

public class BroadCaster extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_broad_caster);
		Intent intent = new Intent();
		intent.setAction("close notification");
		sendBroadcast(intent);
		sendBroadcast(getIntent());
		
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.broad_caster, menu);
		return true;
	}

}
