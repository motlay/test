/*
 * Copyright (C) 2012 Christopher Eby <kreed@kreed.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.mg.mtag;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Framework methods only in Honeycomb or above go here.
 */
@TargetApi(11)
public class CompatHoneycomb {
	/**
	 * Add ActionBar tabs for LibraryActivity.
	 *
	 * @param activity The activity to add to.
	 */
	@SuppressLint("ResourceAsColor")
	public static void addActionBarTabs(final LibraryActivity activity)
	{
		ActionBar.TabListener listener = new ActionBar.TabListener() {
			private final LibraryActivity mActivity = activity;

			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft)
			{
				//setIcon(tab, Integer.parseInt(tab.getContentDescription().toString()), true);
			}

			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft)
			{
				setIcon(tab, Integer.parseInt(tab.getContentDescription().toString()), true);
				mActivity.mViewPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft)
			{
				setIcon(tab, Integer.parseInt(tab.getContentDescription().toString()), false);
			}
		};
		Context context = activity.getApplicationContext();
		ActionBar ab = activity.getActionBar();
		setUpActionBar(ab, context);
		int[] order = activity.mPagerAdapter.mTabOrder;
		int[] titles = LibraryPagerAdapter.TITLES;
		for (int i = 0, n = activity.mPagerAdapter.getCount(); i != n; ++i) {
			int name = titles[order[i]];
			Tab tab = ab.newTab();
			setIcon(tab,order[i],false);
			tab.setText(titles[order[i]]);
			tab.setTabListener(listener);
			int ord = order[i];
			tab.setContentDescription(String.valueOf(ord));
			ab.addTab(tab);
		}
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	}

	private static void setIcon(Tab tab , int tabType,boolean selected) {
		switch (tabType) {
		case GENRE:
			if(selected)
				tab.setIcon(R.drawable.genre_active);
			else
				tab.setIcon(R.drawable.genre_inactive);
			break;
		case ALBUM:
			if(selected)
				tab.setIcon(R.drawable.albums_active);
			else
			tab.setIcon(R.drawable.albums_inactive);
			break;
		case ARTIST:
			if(selected)
				tab.setIcon(R.drawable.artist_active);
			else
			tab.setIcon(R.drawable.artist_inactive);
			break;
		case SONG:
			if(selected)
				tab.setIcon(R.drawable.songs_active);
			else
				tab.setIcon(R.drawable.songs_inactive);
			break;
		case PLAYIST :
			if(selected)
				tab.setIcon(R.drawable.lists_active);
			else
				tab.setIcon(R.drawable.lists_inactive);
			break;
		default:
			break;
		}
	}
	private static void setIcon(ImageView view , int tabType) {
		switch (tabType) {
		case GENRE:
			
			break;
		case ALBUM:
			view.setImageResource(R.drawable.album_icon_light);
			break;
		case ARTIST:
			view.setImageResource(R.drawable.artist_icon_light);
			break;
		case SONG:
			view.setImageResource(R.drawable.song_icon_light);
			break;
		default:
			break;
		}
	}
	/**
	 * Call {@link ListView#setFastScrollAlwaysVisible(boolean)} on the given
	 * ListView with value true.
	 */
	public static void setFastScrollAlwaysVisible(ListView view)
	{
		view.setFastScrollAlwaysVisible(true);
	}

	/**
	 * Call {@link MenuItem#setActionView(View)} on the given MenuItem.
	 */
	public static void setActionView(MenuItem item, View view)
	{
		item.setActionView(view);
	}

	/**
	 * Call {@link MenuItem#setShowAsAction(int)} on the given MenuItem.
	 */
	public static void setShowAsAction(MenuItem item, int mode)
	{
		item.setShowAsAction(mode);
	}

	/**
	 * Select the ActionBar tab at the given position.
	 *
	 * @param activity The activity that owns the ActionBar.
	 * @param position The tab's position.
	 */
	public static void selectTab(Activity activity, int position)
	{
		ActionBar ab = activity.getActionBar();
		if (position < ab.getTabCount()) {
			ab.selectTab(ab.getTabAt(position));
		}
	}

	/**
	 * Call {@link android.provider.MediaStore.Audio.Genres#getContentUriForAudioId(String,int)}
	 * on the external volume.
	 */
	public static Uri getContentUriForAudioId(int id)
	{
		return MediaStore.Audio.Genres.getContentUriForAudioId("external", id);
	}

	/**
	 * Call {@link KeyEvent#hasNoModifiers()}.
	 */
	public static boolean hasNoModifiers(KeyEvent event)
	{
		return event.hasNoModifiers();
	}

	/**
	 * Call {@link KeyEvent#hasModifiers(int)}.
	 */
	public static boolean hasModifiers(KeyEvent event, int modifiers)
	{
		return event.hasModifiers(modifiers);
	}
	public static void setUpActionBar(ActionBar ab,final Context context){
		LayoutInflater   mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 	View layout = (RelativeLayout) mInflater.inflate(R.layout.top_bar,null);
	 	ab.setDisplayUseLogoEnabled(false);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowTitleEnabled(false);
		ab.setIcon(android.R.color.transparent);
	 	OnClickListener listener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, MtagScreen.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
				
			}
		};
		//ImageView aboutUs = (ImageView) layout.findViewById(R.id.mtag_title_about);
		
	 	ImageView imageView = (ImageView) layout.findViewById(R.id.imageView1);
	 	TextView textView = (TextView) layout.findViewById(R.id.history);
	 	textView.setTypeface(Typeface.createFromAsset(context.getAssets(), "Roboto/Roboto-Light.ttf"));
	 	textView.setOnClickListener(listener);
	 	textView.setTextColor(Color.GRAY);
	 	textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
	 	imageView.setOnClickListener(listener);
	 	ab.setCustomView(layout);
	}
	
	
	private static final  int SONG = MediaUtils.TYPE_SONG;
	private static final  int ARTIST = MediaUtils.TYPE_ARTIST;
	private static final  int ALBUM = MediaUtils.TYPE_ALBUM;
	private static final  int GENRE = MediaUtils.TYPE_GENRE;
	private static final int PLAYIST = MediaUtils.TYPE_PLAYLIST;
}
