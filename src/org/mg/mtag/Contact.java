package org.mg.mtag;

public class Contact {

	public  String name;
	public String number;
	public String photoId;

	public Contact(String id,String name , String number){
		this.photoId = id;
		this.name = name;
		this.number = number;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNumber() {
		return number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	public String getPhotoId() {
		return photoId;
	}


	public void setPhotoId(String id) {
		this.photoId = id;
	}


}
