package org.mg.mtag;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ContactsAdapter extends BaseAdapter{
	private Context context;
	private Cursor cursor;
	private List<Contact> contactsList;
	private Cursor friendsCursor;
	private List<String> friendNumbers;
	public ContactsAdapter(Context context,List<Contact> contacts ,Cursor allCursor,Cursor friendsCursor) {
		this.context = context;
		this.cursor = allCursor;
		this.contactsList = contacts;
		this.friendsCursor = friendsCursor;
		this.friendNumbers = new ArrayList<String>();
	}

	@Override
	public int getCount() {
		return contactsList.size();
	}

	@Override
	public Object getItem(int position) {
		//TODO
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView!=null? 
				convertView:inflater.inflate(R.layout.content_item, parent, false);
		String name = null;
		String to = null;
		String photoId= null;
		if(contactsList.size() < position)
			return rowView;
		Contact contact = contactsList.get(position);
		to = contact.getNumber();
		name = contact.getName();
		photoId = contact.getPhotoId();
		TextView view = (TextView) rowView.findViewById(R.id.contactName);
		view.setText(name);
		TextView numberView = (TextView) rowView.findViewById(R.id.numberView);
		numberView.setText(to);
		return rowView;
	}

	private Cursor moveCursorTouniqueContact() {
		String to = null;
		while (to == null){
			to = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));
			if(friendNumbers.contains(to)){
				to = null;
				cursor.moveToNext();
			}else
				break;

		}
		return cursor;
	}

	public void filter(CharSequence cs) {


	}

}
