package org.mg.mtag;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.motlay.mtag.server.DownloadSong;
import com.motlay.mtag.server.MtagUtils;
import com.motlay.mtag.server.NotificationHandler;

public class DedicationScreen extends Activity {

	private String songPath;
	private int SIMPLE_NOTFICATION_ID;
	private String dedicationId ;
	private String songName;
	private String fromName;
	private NotificationHandler handler;
	private Context context;
	private static  Typeface typeface= null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dedication_recieve);
		context = this;
		ActionBar ab = getActionBar();
		ab.setLogo(R.drawable.mtag_title);
		ab.setHomeButtonEnabled(true);
		ab.setTitle("");
		initViews();
	}

	private void initViews() {
		typeface = Typeface.createFromAsset(getAssets(), "Roboto/Roboto-Light.ttf");
		Intent intent = getIntent();
		final String songName = intent.getStringExtra("songName");
		fromName = intent.getStringExtra("name");
		String sanitizedSongName ;
		final String dedicationId = intent.getStringExtra("dedicationId");
		this.songName = songName;
		sanitizedSongName = songName.replaceAll(" ", "%20");
		this.songPath = "http://69.164.201.52/mtagSongs/"+sanitizedSongName+".mp3";
		this.dedicationId = dedicationId;
		handler = new NotificationHandler(this , null , songName , false);
		TextView songNameView = (TextView) findViewById(R.id.trackName);
		TextView personName = (TextView) findViewById(R.id.frndName);
		songNameView.setTypeface(typeface);
		personName.setTypeface(typeface);
		Button cancel = (Button) findViewById(R.id.closeSong);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		
		String formattedSongName = this.songName;
		if(fromName != null && songName != null){
			fromName = Character.toString(fromName.charAt(0)).toUpperCase()+fromName.substring(1);
			formattedSongName = Character.toString(formattedSongName.charAt(0)).toUpperCase()+formattedSongName.substring(1);
		}
		songNameView.setText(formattedSongName +".mp3  ");
		personName.setText(fromName  +  "  Sent");
		Button accept = (Button) findViewById(R.id.acceptSong);
		accept.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(MtagUtils.isInternetAvialable(context)){
					NotificationManager manger = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
					DownloadSong down = new DownloadSong(context,manger,songName,fromName,DedicationScreen.this.dedicationId);
					down.execute("");
					finish();
				}else {
					Toast.makeText(context, "Internet is not avialable Please enable data then try again", Toast.LENGTH_LONG).show();
				}
			}
		});
		
	}

	public void showDialog(){
		new AlertDialog.Builder(this)
		.setTitle("Downloading....")
		.setMessage("Your Song is being Downloaded")
		.setInverseBackgroundForced(true)
		.setPositiveButton("Close", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) { 
				finish();
			}
		})
		.show();
	}

//	public class DownloadSong extends AsyncTask<Object, Object, Object>{
//		private DownloadNotificationHandler handlerSync;
//		public DownloadSong(String song){
//			songPath = song;
//			handler = handlerSync;
//			
//		}
//		public DownloadSong() {
//		}
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			handler.sendNotification();
//		}
//
//
//		@Override
//		protected Object doInBackground(Object... params) {
//			try {
//				File root = android.os.Environment.getExternalStorageDirectory();
//				File dir = new File(root.getAbsolutePath() + "/Music/");
//				if(dir.exists() == false){
//					dir.mkdirs();  
//				}
//				URL url = new URL(songPath);
//				File file = new File(dir,songName+".mp3");
//
//				URLConnection uconn = url.openConnection();
//				int fileLength = uconn.getContentLength();
//				showToast(fileLength);
//				InputStream is = uconn.getInputStream();
//				BufferedInputStream bufferinstream = new BufferedInputStream(is);
//				ByteArrayBuffer baf = new ByteArrayBuffer(20000);
//				int current = 0;
//				int count = 0;
//				while((current = bufferinstream.read()) != -1){
//					count = count + current; 
//					int progress = ((count / fileLength)) ;
//					if(progress > 99)
//						progress = 99;
//					publishProgress(progress);
//					baf.append((byte) current);
//				}
//				FileOutputStream fos = new FileOutputStream(file);
//				fos.write(baf.toByteArray());
//				fos.flush();
//				fos.close();
//				trackPath = file.getAbsolutePath();
//			}catch (Exception e){
//				System.out.println(e.getStackTrace());
//			}
//			return null;
//		}
//
//		@Override
//		protected void onProgressUpdate(Object... values) {
//			super.onProgressUpdate(values);
//			handler.updateNotification((Integer) values[0]);
//		}
//
//		@Override
//		protected void onPostExecute(Object result) {
//			handler.cancelNotification();
//			NotificationManager mNotificationManager = 
//					(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
//			final Notification notifyDetails = 
//					new Notification(R.drawable.mtag_logo,
//							"MTAG Song Download!",System.currentTimeMillis());
//			Context context = getApplicationContext();
//			CharSequence contentTitle = 
//					"MTAG";
//			CharSequence contentText = 
//					songName +"Click here to view";
//			Intent notifyIntent = 
//					new Intent(getApplicationContext() , PlayDedication.class);
//			notifyIntent.putExtra("songPath",trackPath);
//			notifyIntent.putExtra("songName",songName);
//
//			PendingIntent intent = PendingIntent.getActivity(context, 0, notifyIntent, 0);
//			notifyDetails.setLatestEventInfo(context, 
//					contentTitle, contentText, intent);
//			notifyDetails.flags |= Notification.FLAG_AUTO_CANCEL;
//			notifyDetails.sound =  RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//			mNotificationManager.notify(SIMPLE_NOTFICATION_ID, notifyDetails);
//
//			super.onPostExecute(result);
//		}
//	}
//
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.dedication_screen, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		return super.onOptionsItemSelected(item);
	}

	public void showToast(final int fileLength) {
		this.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(context, "Downloading Song " + songName + "  ("+ MtagUtils.formatFileSize(fileLength) + " )" , Toast.LENGTH_LONG).show();
			}
		});

	}

}
