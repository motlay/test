package org.mg.mtag;

import java.util.HashMap;
import java.util.Map;

import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;
import com.motlay.mtag.server.FileUpload;
import com.motlay.mtag.server.MtagUtils;

public class FriendSelect extends Activity {
	
	private FileUpload sendSong ;
	private ListView listView;
	private Cursor friendCursor;
	private static  Typeface typeface= null;
	private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		  @Override
		  public void onReceive(Context context, Intent intent) {
		    // Extract data included in the Intent
			  sendSong.cancel(true);
		  }
		};
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		typeface = Typeface.createFromAsset(getAssets(), "Roboto/Roboto-Light.ttf");
		setContentView(R.layout.activity_friend_select);
		listView = (ListView) findViewById(R.id.friendsContact);
		friendCursor = getFriendsContacts();
		FriendsContactAdapter adapter = new FriendsContactAdapter(this, friendCursor);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				sendTo(arg2);
			}
		});
		registerReceiver(mMessageReceiver, new IntentFilter("org.kreed.vanilla.FriendSelect.MyPhoneReceiver"));
	}
	
	protected void sendTo(int position ) {
			friendCursor.moveToPosition(position);
			String toUser = friendCursor.getString(2);
			String personName = friendCursor.getString(1);
			SQLiteDatabase db = this.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.CREATE_IF_NECESSARY , null);
			Cursor fromUserCursor = DsSQLLite.getFieldFromFirstRow(db, DatabaseConstants.ACCOUNT, DatabaseConstants.MTAG_ID);
			fromUserCursor.moveToFirst();
			String fromUser = fromUserCursor.getString(0);
			Intent intent = getIntent();
			String songPath = intent.getStringExtra("fileName");
			Map<String,String > info = new HashMap<String, String>();
			info.put("fileName", songPath);
			info.put("firstName",personName);
			info.put("from", fromUser);
			info.put("to", toUser);
			String songName = intent.getStringExtra("songName");
			info.put("songName",songName );
			String songSize = intent.getStringExtra("songSize");
			if(MtagUtils.isInternetAvialable(this)){
				addToUpload(db,songName, songPath, fromUser, toUser , DatabaseConstants.TRUE);
				Toast.makeText(this, "Uploading Song " + songName + "  ("+ songSize + " )" , Toast.LENGTH_LONG).show();
				 sendSong = new FileUpload(getApplicationContext(),info);
				sendSong.execute("");
			}else {
				Toast.makeText(this, "Ineternet Not Avialable song added to upload queue Song Name -:" +songName, Toast.LENGTH_LONG).show();
				addToUpload(db,songName, songPath, fromUser, toUser , DatabaseConstants.FALSE);
			}
			Toast.makeText(this, "Uploading Song " + songName + "  ("+ songSize + " )" , Toast.LENGTH_LONG).show();
			db.close();
			finish();
		}
		private void addToUpload(SQLiteDatabase db,String songName , String songPath , String fromUser , String toUser ,String uploaded) {
			if(!MtagUtils.isTableExists(DatabaseConstants.SHARED, db)){
				DsSQLLite.execute(db, DatabaseConstants.CREATE_TABLE_SHARED);
			}
			DsSQLLite.insert(db, DatabaseConstants.SHARED, toUser , fromUser , songPath ,songName ,uploaded,String.valueOf(System.currentTimeMillis()));
		}
			

	
	public Cursor getFriendsContacts(){
		SQLiteDatabase db = this.openOrCreateDatabase(DatabaseConstants.MTAG, SQLiteDatabase.CREATE_IF_NECESSARY, null);
		Cursor friendsCursor = null;
		try {
			//friendsCursor = DsSQLLite.getFieldFromFirstRow(db, DatabaseConstants.ACCOUNT, DatabaseConstants.PHONE_NO);
			String query = "Select * from contact";
			friendsCursor = DsSQLLite.rawQuery(db, query);
		}catch (Exception e){
			return friendsCursor;
		}
		return friendsCursor;
	}
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.friend_select, menu);
		ActionBar ab = getActionBar();
		TextView upTextView = (TextView) getLayoutInflater().inflate(
		        R.layout.up_text, null);
		upTextView.setText("Back");
		upTextView.measure(0, 0);
		upTextView.setTypeface(typeface);
		upTextView.layout(0, 0, upTextView.getMeasuredWidth(), 
		        upTextView.getMeasuredHeight());
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowTitleEnabled(false);
		ab.setCustomView(upTextView);
		
		ab.setTitle("");
		ab.setHomeButtonEnabled(true);
		upTextView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
		
		return true;
	}
	
	
	
}
