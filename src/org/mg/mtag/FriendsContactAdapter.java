package org.mg.mtag;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FriendsContactAdapter extends BaseAdapter {

	private Context context ; 
	private Cursor cursor;
	public FriendsContactAdapter(Context context , Cursor cursor){
		this.context = context;
		this.cursor = cursor;
		
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(cursor == null)
			return 0;
		return cursor.getCount();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView!=null? 
				convertView:inflater.inflate(R.layout.content_item, parent, false);
		cursor.moveToPosition(position);
		String mtagId = cursor.getString(0);
		String name = cursor.getString(1);
		String phoneNo = cursor.getString(2);
		TextView view = (TextView) rowView.findViewById(R.id.contactName);
		view.setText(name);
		TextView numberView = (TextView) rowView.findViewById(R.id.numberView);
		numberView.setText(phoneNo);
		return rowView;
	}

}
