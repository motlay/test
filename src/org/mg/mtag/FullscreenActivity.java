package org.mg.mtag;

import java.util.HashMap;
import java.util.Map;

import org.kreed.vanilla.util.SystemUiHider;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Gravity;
import android.widget.Toast;

import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;
import com.motlay.mtag.server.FileUpload;
import com.motlay.mtag.server.MtagUtils;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_activity);
		Intent intent = new Intent( Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI );
		intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
		startActivityForResult(intent, 1);
	}
	
	@Override
	public void onBackPressed() {
	    // TODO Auto-generated method stub
	    super.onBackPressed();
	    setResult(Activity.RESULT_CANCELED);
	    finish();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
	
		Intent intent = getIntent();
		String songPath = intent.getStringExtra("fileName");
		String songName = intent.getStringExtra("songName");
		String songSize = intent.getStringExtra("songSize");
		if(resultCode == Activity.RESULT_OK )
		{
			SQLiteDatabase db = this.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.CREATE_IF_NECESSARY , null);
			Cursor fromUserCursor = DsSQLLite.getFieldFromFirstRow(db, DatabaseConstants.ACCOUNT, DatabaseConstants.MTAG_ID);
			fromUserCursor.moveToFirst();
			String fromUser = fromUserCursor.getString(0);
			fromUserCursor.close();
		
			Map<String , String> toUser = getToUser(data);
			if(toUser == null){
				Toast toast = Toast.makeText(this,"Invalid Contact selected Please try again .", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				Intent intents = getIntent();
				startActivity(intents);
				
			}else{

				Map<String , String> info = new HashMap<String, String>();
				info.put("fileName", songPath);
				info.put("songName", songName);
				info.put("from", fromUser);
				info.put("to", toUser.get("phoneNo"));
				info.put("firstName", toUser.get("name"));
				if(MtagUtils.isInternetAvialable(this)){
					FileUpload sendSong = new FileUpload(getApplicationContext(),info);
					sendSong.execute("");

					addToUpload(db,songName, songPath, fromUser, toUser.get("phoneNo") , DatabaseConstants.TRUE);
					Toast toast = Toast.makeText(this, "Uploading Song " + songName + "  ("+ songSize + " )" , Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					
				}else {
					Toast toast = Toast.makeText(this, "Ineternet Not Avialable song added to upload queue Song Name -:" +songName, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					addToUpload(db,songName, songPath, fromUser, toUser.get("phoneNo") , DatabaseConstants.FALSE);
					finish();
				}
				finish();
			}
		}else if(resultCode == Activity.RESULT_CANCELED ){
			Intent newIntent = new Intent(getApplicationContext(), Select.class);
			newIntent.putExtra("fileName", songPath);
			newIntent.putExtra("songName", songName);
			newIntent.putExtra("songSize", songSize);
			newIntent.putExtra("tried", true);
			startActivity(newIntent);
			finish();
		}
	}

	private Map<String , String> getToUser(Intent data ) {
		String phoneNo = null ;
		Uri uri = data.getData();
		Map<String , String> userData = new HashMap<String, String>();
		Cursor cursor = getContentResolver().query(uri, null, null, null, null);
		cursor.moveToFirst();
		int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
		int nameIndex = cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
		phoneNo = cursor.getString(phoneIndex);
		if(phoneNo == null) {
			phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
			phoneNo = cursor.getString(phoneIndex);
		}
		if(phoneNo.length() == 10 || phoneNo.length() == 12 || phoneNo.length() == 13){
			userData.put("phoneNo", phoneNo);
			userData.put("name", cursor.getString(nameIndex));
			return userData;
		}
		else
			return null;
	}
	private void addToUpload(SQLiteDatabase db,String songName , String songPath , String fromUser , String toUser ,String uploaded) {
		if(!MtagUtils.isTableExists(DatabaseConstants.SHARED, db)){
			DsSQLLite.execute(db, DatabaseConstants.CREATE_TABLE_SHARED);
		}
		DsSQLLite.insert(db, DatabaseConstants.SHARED, toUser , fromUser , songPath ,songName ,uploaded,String.valueOf(System.currentTimeMillis()));
		db.close();
	}


}
