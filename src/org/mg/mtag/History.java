package org.mg.mtag;

import android.app.ActionBar;
import android.app.TabActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class History extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);
		ActionBar ab = getActionBar();
		ab.setLogo(R.drawable.mtag_title);
		ab.setHomeButtonEnabled(true);
		ab.setTitle("");
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.history, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case  android.R.id.home:
			finish();
			return true;
		default :
			return true;
		}
	}

}
