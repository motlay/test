package org.mg.mtag;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.analytics.tracking.android.EasyTracker;
import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;
import com.mg.util.HTTPConstants;
import com.motlay.mtag.server.MtagUtils;

public class LoginActivity extends Activity {


	private static String phoneNo;
	private static String imei ;
	private static JSONObject responseJson =null;
	private static String version = android.os.Build.VERSION.RELEASE; 
	private static ProgressDialog progressDialog;
	private static String url = "http://69.164.201.52:9998/register/user";
	private static Context contextGlobal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		ActionBar ab = getActionBar();
		ab.setLogo(R.drawable.mtag_title);
		ab.setTitle("");
		boolean network = MtagUtils.isInternetAvialable(getApplicationContext());
		contextGlobal = this;

		if(network)
			intitLogin();
		else
			askForNetwork();
	}


	private void askForNetwork() {
		new AlertDialog.Builder(this).setTitle("Network Problem")
		.setMessage("Network connection Required For First Time Users")
		.setPositiveButton("Restart", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
				startActivity(intent);
				finish();
			}
		})
		.setNegativeButton("Close", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) { 
				finish();
			}
		})
		.show();
	}



	private void intitLogin() {
		EditText phoneValue = (EditText) findViewById(R.id.phoneNumber);
		phoneValue.setFocusable(true);
		phoneValue.requestFocus();
		TelephonyManager   telephonyManager  =  ( TelephonyManager     
				)getSystemService( Context.TELEPHONY_SERVICE );

		imei = telephonyManager.getDeviceId();
		Button submit = (Button) findViewById(R.id.button1);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				EditText phone   = (EditText)findViewById(R.id.phoneNumber);
				String countryCode = "91";
				InputMethodManager inputManager = (InputMethodManager)
						getSystemService(Context.INPUT_METHOD_SERVICE); 
				inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
				authenticate(phone.getText().toString(),countryCode);
			}
		});

	}

	private void authenticate(String phoneNumber, String countryCode) {
		if(phoneNumber.startsWith("+"))
			phoneNumber = phoneNumber.substring(1);
		phoneNo =  phoneNumber;
		ServerVerify verify = new ServerVerify(this,new TaskCallback() {

			@Override
			public void done() {
				finish();

			}
		});
		verify.execute("");
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage("Registering.....");
		progressDialog.show();
	}
	public static class ServerVerify extends  AsyncTask<Object, Object, Object>{
		Context context;
		private TaskCallback mCallback;
		private ServerVerify(Context appContext , TaskCallback callback) {
			context = appContext.getApplicationContext();
			mCallback = callback;

		}
		@Override
		protected Object doInBackground(Object... param) {
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
			nameValuePairs.add(new BasicNameValuePair(HTTPConstants.PHONE_NUMBER , phoneNo));
			nameValuePairs.add(new BasicNameValuePair(HTTPConstants.IMEI, imei));
			nameValuePairs.add(new BasicNameValuePair(HTTPConstants.OS_TYPE, "android"));
			nameValuePairs.add(new BasicNameValuePair(HTTPConstants.OS_VERSION, version));

			try {
				post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}  catch (UnsupportedEncodingException e) {
				Log.e("UnsupportedEncodingException", "could not load url while registering user");
			}
			HttpResponse response = null;
			try {
				response =  client.execute(post);
			} catch (ClientProtocolException e) {
				Log.e("ClientProtocolException", "Error while registering user");
			} catch (IOException e) {	
				Log.e("IOException", "Error while registering user");
			}
			return response;
		}
		@Override
		protected void onProgressUpdate(Object... values) {
			super.onProgressUpdate(values);
		}
		@Override
		protected void onPostExecute(Object result) {
			responseJson = MtagUtils.readResponse((HttpResponse)result);
			try {
				startLogin(context);
				mCallback.done();
			} catch (JSONException e) {
				Log.e("Json Exception", "Error in reading json response while registering User");
			}
		}


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this,SplashScreen.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra("status", false);
		startActivity(intent);
		finish();
		super.onBackPressed();	
	}

	public static void startLogin(Context context) throws JSONException {
		if(responseJson == null){
			Intent intent = new Intent(contextGlobal, SplashScreen.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		}
		String status = responseJson.getString(HTTPConstants.STATUS);
		String mtagId = responseJson.getString(HTTPConstants.MTAG_ID);
		String number = responseJson.getString(HTTPConstants.PHONE_NUMBER);
		String joinedOn = responseJson.getString(HTTPConstants.JOINED_ON);
		boolean hasFriends = responseJson.getBoolean("hasFriends");
		progressDialog.dismiss();
		if(status.equals("1") && !mtagId.isEmpty()){
			saveInDatabase(context,mtagId,number,joinedOn);
			progressDialog.dismiss();
			Intent intent = new Intent(contextGlobal, NumberVerify.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra("mtagId", mtagId);
			if(hasFriends){
				intent.putExtra("hasFriends", true);
			}
			context.startActivity(intent);
		}

	}

	private static void saveInDatabase(Context context ,String mtagId,String number , String joinedOn ) {
		SQLiteDatabase db = context.openOrCreateDatabase("mtag",SQLiteDatabase.CREATE_IF_NECESSARY , null);
		if(!MtagUtils.isTableExists(DatabaseConstants.ACCOUNT,db)){
			String ACCOUNT_TABLE = "create table " 
					+ DatabaseConstants.ACCOUNT
					+ "(" 
					+ DatabaseConstants.MTAG_ID + " text not null, " 
					+ DatabaseConstants.PHONE_NO + " text not null, " 
					+ DatabaseConstants.JOINED_ON + " text not null,"
					+ DatabaseConstants.VERIFIED + " text not null"
					+ ");";
			DsSQLLite.execute(db, ACCOUNT_TABLE);
		}
		DsSQLLite.insert(db, DatabaseConstants.ACCOUNT, mtagId, number , joinedOn ,"false" );
		db.close();
	}
	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); 
	}
	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); 
	}
	private static final String COUNTRY_CODE = "91";

}
