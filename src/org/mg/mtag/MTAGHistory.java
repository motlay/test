package org.mg.mtag;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;
import android.widget.TextView;

import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;
import com.motlay.mtag.history.HistoryAdapter;
import com.motlay.mtag.history.SongRecieved;
import com.motlay.mtag.history.SongSended;
import com.motlay.mtag.server.MtagUtils;

public class MTAGHistory extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mtaghistory);
		//setSongSendView();
		ActionBar ab = getActionBar();
        ab.setHomeButtonEnabled(true);
		ab.setLogo(R.drawable.mtag_title);
		ab.setTitle("");
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		Tab tab = ab.newTab();	
		tab.setText("Song Send");
		
		tab.setTabListener(new TabListener() {
			
			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				
				
			}
			
			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				Intent intent = new Intent(getApplicationContext(), SongSended.class);
				startActivity(intent);
			}
			
			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}
		});
		
		Tab tab2 = ab.newTab();
		tab2.setText("Song Recieved");
		tab2.setTabListener(new TabListener() {
			
			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				
			}
			
			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				Intent intent = new Intent(getApplicationContext(), SongRecieved.class);
				startActivity(intent);
			}
			
			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}
		});
		ab.addTab(tab);
		ab.addTab(tab2);
		ab.selectTab(tab);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		return true;
	}
	protected void setSongSendView() {
		ListView listView = (ListView) findViewById(R.id.songRecieved);
		Cursor cursor = getCursor(); 
		if(cursor == null){
			TextView emptyView = (TextView) findViewById(R.id.emptySend);
			emptyView.setVisibility(1);
			listView.setEmptyView(emptyView);
		}else{
			listView.setAdapter(new HistoryAdapter(getApplicationContext(), cursor));
		}

	}
	private Cursor getCursor() {

		SQLiteDatabase db = getApplicationContext().openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.OPEN_READWRITE, null);
		Cursor cursor = null ;
		if(MtagUtils.isTableExists(DatabaseConstants.SHARED, db)){
			String query = "Select * from "+DatabaseConstants.SHARED ;
			cursor  =  DsSQLLite.rawQuery(db, query);
		}
		return cursor;
	}
}
