package org.mg.mtag;

import android.app.ActionBar;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.motlay.mtag.history.SongRecieved;
import com.motlay.mtag.history.SongSended;

public class MtagScreen extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mtag_screen);
			setUpActionBar(getActionBar(), getApplicationContext());
			TabWidget widget = getTabWidget();
			widget.setFocusable(true);
			widget.setStripEnabled(false);
			
	        Resources res = getResources(); 
	        TabHost tabHost = getTabHost();
	        tabHost.setFocusable(true);
	        TabHost.TabSpec spec; 
	        Intent history ;
	        history = new Intent(this,SongSended.class);
	     
	        View tabview = createTabView(tabHost.getContext(), "Sent");

	        TabSpec setContent = tabHost.newTabSpec( "Sent").setIndicator(tabview).setContent(history);
	        tabHost.addTab(setContent);
	       
	        Intent intent; 
	        intent = new Intent().setClass(this, SongRecieved.class);
	        
	        View tabview2 = createTabView(tabHost.getContext(), "Recieved");

	        TabSpec spec2 = tabHost.newTabSpec( "Recieved").setIndicator(tabview2).setContent(intent);
       
	       
	        tabHost.addTab(spec2);


	        tabHost.setCurrentTab(2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.mtag_screen, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case  android.R.id.home:
			finish();
			return true;
		default :
			return true;
		}
	}

	 private static View createTabView(final Context context, final String text) {
         View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
         TextView tv = (TextView) view.findViewById(R.id.tabsText);
         tv.setText(text);
         return view;
 }
		public  void setUpActionBar(ActionBar ab,final Context context){
			RelativeLayout layout = (RelativeLayout) getLayoutInflater().inflate(
					R.layout.up_text, null);
			TextView upTextView = (TextView) layout.findViewById(R.id.actionbar_text);
			upTextView.setText("Collection");
			upTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
			upTextView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto/Roboto-Light.ttf"));
			upTextView.setTextColor(Color.GRAY);
			ab.setDisplayUseLogoEnabled(false);
			ab.setDisplayShowCustomEnabled(true);
			ab.setDisplayShowTitleEnabled(false);
			ab.setIcon(R.drawable.back);
			ab.setCustomView(layout);
			upTextView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					finish();
				}
			});
		}
		@Override
		protected void onStart() {
			super.onStart();
			EasyTracker.getInstance(this).activityStart(this); 
		}
		@Override
		protected void onStop() {
			super.onStop();
			EasyTracker.getInstance(this).activityStop(this); 
		}
}