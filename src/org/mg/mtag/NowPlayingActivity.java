package org.mg.mtag;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;

public class NowPlayingActivity extends Activity {

	ListView playListView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_now_playing);
		
		ArrayList<String> playlist = getIntent().getStringArrayListExtra("songsList");
		if(playlist != null)
			intitViews((ArrayList<String>) playlist);
		
	}
	
	private void intitViews(ArrayList<String> playlist) {
		playListView = (ListView) findViewById(R.id.nowPlaying);
		NowPlayingAdapter adapter = new NowPlayingAdapter(getApplicationContext() , playlist);
		playListView.setAdapter(adapter);
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.now_playing, menu);
		return true;
	}

}
