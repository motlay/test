package org.mg.mtag;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NowPlayingAdapter extends BaseAdapter{

	private static ArrayList<String> songs;
	private Context context;
	private final LibraryActivity mActivity;


	public  NowPlayingAdapter(Context context , ArrayList<String> songs ){
		this.songs = songs;
		this.context = context;
		this.mActivity = new LibraryActivity();
	}
	@Override
	public int getCount() {
		if(songs == null)
			return 0;
		return songs.size();
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private static class ViewHolder {
		public long id;
		public String title;
		public TextView text;
		public ImageView sendSong;
		public String songPath;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		int songsListSize = songs.size();
		LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(position <= songsListSize){
			ViewHolder holder;
			String path  = String.valueOf(songs.get(position));
			Uri uri = Uri.withAppendedPath( MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
					path);
			Cursor cursor = context.getContentResolver().query(uri, FILLED_PROJECTION, null, null, null);
			if(cursor != null){
				cursor.moveToFirst();
				int music_column_index = cursor
						.getColumnIndexOrThrow(MediaStore.Audio.Media._ID);
				long songId = cursor.getLong(music_column_index);
				Song song = new Song(songId);
				song.populate(cursor);
				LinearLayout lay = null;
				View view ;
				android.view.View.OnClickListener listener = OnClickListener();
				int layout =  R.layout.now_playing_content;
				view = 	inflator.inflate(layout, null);
				holder = new ViewHolder();
				view.setTag(holder);
				holder.text = (TextView)view.findViewById(R.id.songName);
				holder.sendSong = (ImageView) view.findViewById(R.id.now_song_send);
				holder.text.setTag(holder);
				holder.text.setMinimumWidth(120);
				holder.songPath = String.valueOf(songs.get(position));
				holder.sendSong.setOnClickListener(listener);
				holder.text.setOnClickListener(listener);
				holder.sendSong.setRight(1);
				holder.sendSong.setTag(holder);
				holder.sendSong.setOnClickListener(listener);
				holder.id = cursor.getLong(0);
				String line1 = cursor.getString(2);
				String line2 = cursor.getString(3);
				SpannableStringBuilder sb = new SpannableStringBuilder(line1);
				sb.append('\n');
				sb.append(line2);
				sb.setSpan(new ForegroundColorSpan(Color.GRAY), line1.length() + 1, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				holder.text.setText(sb);
				holder.title = line1;
				cursor.close();
				return view;


			}
		}
		return null;
	}


	private android.view.View.OnClickListener OnClickListener() {
		android.view.View.OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(View view) {
				int id = view.getId();
				 if (id == R.id.songName){
					Intent intent = createData(view);
					intent.putExtra("type", MediaUtils.TYPE_SONG);
					intent.putExtra("play", true);
					ViewHolder holder = (ViewHolder) view.getTag();
					String path = holder.songPath;
					Uri uri = Uri.withAppendedPath( MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
							path);
					Cursor cursor = context.getContentResolver().query(uri, FILLED_PROJECTION, null, null, null);
					if(cursor != null){
						cursor.moveToFirst();
						int music_column_index = cursor
								.getColumnIndexOrThrow(MediaStore.Audio.Media._ID);
						long songId = cursor.getLong(music_column_index);
						Song song = new Song(songId);
						song.populate(cursor);
						cursor.close();
						mActivity.onItemClicked(intent , context,song);
					}
				}else if(id == R.id.now_song_send){
					Intent intent = createData(view);
					intent.putExtra("type", MediaUtils.TYPE_SONG);
					intent.putExtra("send", true);
					ViewHolder holder = (ViewHolder) view.getTag();
					String path = holder.songPath;
					Uri uri = Uri.withAppendedPath( MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
							path);
					Cursor cursor = context.getContentResolver().query(uri, FILLED_PROJECTION, null, null, null);
					if(cursor != null){
						cursor.moveToFirst();
						int music_column_index = cursor
								.getColumnIndexOrThrow(MediaStore.Audio.Media._ID);
						long songId = cursor.getLong(music_column_index);
						Song song = new Song(songId);
						song.populate(cursor);
						cursor.close();
						mActivity.onItemClicked(intent , context,song);
				}
			}
		}
		};
		return listener;
	}
	public Intent createData(View view)
	{
		ViewHolder holder = (ViewHolder)view.getTag();
		Intent intent = new Intent();
		intent.putExtra(LibraryAdapter.DATA_TYPE, MediaUtils.TYPE_SONG);
		intent.putExtra(LibraryAdapter.DATA_ID, holder.id);
		intent.putExtra(LibraryAdapter.DATA_TITLE, holder.title);
		intent.putExtra(LibraryAdapter.DATA_EXPANDABLE, false);
		return intent;
	}
	public static final String[] FILLED_PROJECTION = {
		MediaStore.Audio.Media._ID,
		MediaStore.Audio.Media.DATA,
		MediaStore.Audio.Media.TITLE,
		MediaStore.Audio.Media.ALBUM,
		MediaStore.Audio.Media.ARTIST,
		MediaStore.Audio.Media.ALBUM_ID,
		MediaStore.Audio.Media.ARTIST_ID,
		MediaStore.Audio.Media.DURATION,
		MediaStore.Audio.Media.TRACK,
		MediaStore.Audio.Media.SIZE,
	};

}
