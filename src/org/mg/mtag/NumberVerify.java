package org.mg.mtag;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mg.util.DatabaseConstants;
import com.motlay.mtag.server.MtagUtils;

public class NumberVerify extends Activity {

	private static Context contextGlobal ;
	private static String url = "http://69.164.201.52:9998/user/verify";
	private static ProgressDialog progressDialog;
	private static  String mtagId = null;
	private static String verification_code = null;
	private static JSONObject responseJson;
	private static boolean hasFriends = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_number_verify);
		contextGlobal = getApplicationContext();
		mtagId = getIntent().getStringExtra("mtagId");
		hasFriends = getIntent().getBooleanExtra("hasFriends", false);
		verification_code = "1234";
		ActionBar ab = getActionBar();
		ab.setLogo(R.drawable.mtag_title);
		ab.setTitle("");
		
		Button btn = (Button) findViewById(R.id.veri_btn);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditText editText = (EditText) findViewById(R.id.veri_code);
				verification_code = editText.getText().toString();
				if(verification_code == null || verification_code.isEmpty() || verification_code.length() != 4){
					Toast.makeText(contextGlobal, "Please enter verfication code received via message", Toast.LENGTH_LONG).show();;
				}else{
					verify_number();
				}
				
			}
		});
	}
	
	protected void verify_number() {
		NumberVerfication friends = new NumberVerfication(getApplicationContext(), new TaskCallback() {
			
			@Override
			public void done() {
				finish();
				
			}
		});
		friends.execute("");
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage("Verifying Number Please Wait ....");
		progressDialog.show();
	}
	private static  void startLibraryActivity(){
		Intent intent = new Intent(contextGlobal, LibraryActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		contextGlobal.startActivity(intent);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.number_verify, menu);
		return true;
	}

	
	public static class NumberVerfication extends  AsyncTask<Object, Object, Object>{
		Context context;
		private TaskCallback mCallback;
		private NumberVerfication(Context appContext , TaskCallback callback) {
			context = appContext.getApplicationContext();
			mCallback = callback;

		}
		@Override
		protected Object doInBackground(Object... param) {
				DefaultHttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair("mtagId" , mtagId));
				nameValuePairs.add(new BasicNameValuePair("code", verification_code));
				
				try {
					post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				}  catch (UnsupportedEncodingException e) {
					Log.e("UnsupportedEncodingException", "could not load url while registering user");
				}
				HttpResponse response = null;
				try {
					response =  client.execute(post);
				} catch (ClientProtocolException e) {
					Log.e("ClientProtocolException", "Error while registering user");
				} catch (IOException e) {	
					Log.e("IOException", "Error while registering user");
				}
			
				return response;
			
		}
		@Override
		protected void onProgressUpdate(Object... values) {
			super.onProgressUpdate(values);
		}
		@Override
		protected void onPostExecute(Object result) {
			try {
				responseJson = new JSONObject(MtagUtils.readResponse((HttpResponse) result).toString());
			    mCallback.done();
				saveInDatabase(responseJson);
				if(hasFriends){
					Intent intent = new Intent(contextGlobal, RestoreFriends.class);
					intent.putExtra("mtagId", mtagId);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					contextGlobal.startActivity(intent);
				}else{
					startLibraryActivity();
				}
			
			} catch (Exception e) {
				Log.e("Json Exception", "Error in reading json response while registering User");
			}
		}
	}


	public static void saveInDatabase(JSONObject response) throws JSONException{
		if(response != null){
			SQLiteDatabase db = contextGlobal.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.OPEN_READWRITE, null);
			boolean verfied = response.getBoolean("verified");
			if(verfied){
				if(MtagUtils.isTableExists(DatabaseConstants.ACCOUNT, db)){
					String updateQuery = "UPDATE "+DatabaseConstants.ACCOUNT+" SET "+DatabaseConstants.VERIFIED+" ='true' WHERE "+DatabaseConstants.MTAG_ID + " = '" +mtagId+ "'";
					db.execSQL(updateQuery);
				}
			}else{
				Toast.makeText(contextGlobal, "Wrong Code Please try again .", Toast.LENGTH_LONG).show();
				Intent intent = new Intent(contextGlobal, NumberVerify.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				contextGlobal.startActivity(intent);
			}
			db.close();
		}else{
				Toast.makeText(contextGlobal, "Some problem occured Please try again .", Toast.LENGTH_LONG).show();
		}
	}
}


