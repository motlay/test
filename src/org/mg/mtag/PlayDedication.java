package org.mg.mtag;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;

import com.mg.util.AppConstants;

public class PlayDedication extends Activity {

	private  String songTitle = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play_dedication);
		Intent currentIntent = getIntent();
		String trackName = currentIntent.getStringExtra(AppConstants.SONG_NAME);
		songTitle = trackName;
		 final Cursor cursor = getContentResolver().query(
	                MediaStore.Audio.Media.INTERNAL_CONTENT_URI,
	              Song.FILLED_PROJECTION,
	                MediaStore.Audio.Media.TITLE+ "=?",
	                new String[] {songTitle},
	                "LOWER(" + MediaStore.Audio.Media.TITLE + ") ASC");
		  final Cursor mExternalCursor = getContentResolver().query(
	                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
	               Song.FILLED_PROJECTION,
	                MediaStore.Audio.Media.TITLE+ "=?",
	                new String[] {songTitle},
	                "LOWER(" + MediaStore.Audio.Media.TITLE + ") ASC");

	        Cursor[] cursors = {cursor, mExternalCursor};
	        final MergeCursor mMergeCursor = new MergeCursor(cursors);
	        if (mMergeCursor.moveToFirst()) {
	            do {
			long songId = mMergeCursor.getLong(0);
			Song song = new Song(songId);
			song.populate(mMergeCursor);
			openLibrary(song);
	            } while (mMergeCursor.moveToNext());
	        }
	        mMergeCursor.close();
		finish();

	}
	public void openLibrary(Song song)
	{
		Intent intent = new Intent(this, LibraryActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		if (song != null) {
			intent.putExtra("albumId", song.albumId);
			intent.putExtra("album", song.album);
			intent.putExtra("artist", song.artist);
		}
		startActivity(intent);
		finish();
	
	}
	public long getIdFromURI(Uri contentUri) {
		Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
		int column_index =cursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID);
		int titleIndex = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);
		if(cursor != null){
			if(cursor.isBeforeFirst()){
				while(!cursor.isAfterLast()){
					cursor.moveToNext();
					String title = cursor.getString(titleIndex);
					if(title != null || !title.isEmpty()){
						if(songTitle.equalsIgnoreCase(title))
							break;
					}
				}
			}
		}
		return cursor.getLong(column_index);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.play_dedication, menu);
		return true;
	}

}
