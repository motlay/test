package org.mg.mtag;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class RegistrationLoader extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration_loader);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration_loader, menu);
		return true;
	}
	
}
