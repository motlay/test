package org.mg.mtag;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;
import android.view.Menu;

import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;
import com.motlay.mtag.server.MtagUtils;

public class RestoreFriends extends Activity {

	private static Context contextGlobal ;
	private static String url = "http://69.164.201.52:9998/user/getFriend?mtagId=";
	private static ProgressDialog progressDialog;
	private static  String mtagId = null;
	private static JSONObject responseJson;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_restore_friends);
		contextGlobal = getApplicationContext();
		mtagId = getIntent().getStringExtra("mtagId");
		ActionBar ab = getActionBar();
		ab.setLogo(R.drawable.mtag_title);
		ab.setTitle("");
		getFriends(mtagId);
	}
	
	protected void getFriends(String mtagId) {
		GetFriends friends = new GetFriends(getApplicationContext(), new TaskCallback() {
			
			@Override
			public void done() {
				finish();
				
			}
		});
		friends.execute("");
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage("Re-storing friends");
		progressDialog.show();
	}
	private static  void startLibraryActivity(){
		Intent intent = new Intent(contextGlobal, LibraryActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		contextGlobal.startActivity(intent);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.restore_friends, menu);
		return true;
	}

	
	public static class GetFriends extends  AsyncTask<Object, Object, Object>{
		Context context;
		private TaskCallback mCallback;
		private GetFriends(Context appContext , TaskCallback callback) {
			context = appContext.getApplicationContext();
			mCallback = callback;

		}
		@Override
		protected Object doInBackground(Object... param) {
			URL obj = null;
			try {
				obj = new URL(url+mtagId);
			} catch (MalformedURLException e2) {
				Log.e("Friends Restore process", "MalformedURLException while creating url");
			}
			HttpURLConnection con = null;
			try {
				con = (HttpURLConnection) obj.openConnection();
			} catch (IOException e1) {
				e1.printStackTrace();
				Log.e("Friends Restore process", "IO Exception while opening connection" );
			}
			try {
				con.setRequestMethod("GET");
			} catch (ProtocolException e) {
				Log.e("Friends Restore process", "Protocol Exception while Setting method");
			}

			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			StringBuffer response = new StringBuffer();
			BufferedReader in;
			try {
				in = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
			} catch (IOException e) {
				Log.e("Notifications Pull", "IO Exception while reading response");
			}
			return response;
		}
		@Override
		protected void onProgressUpdate(Object... values) {
			super.onProgressUpdate(values);
		}
		@Override
		protected void onPostExecute(Object result) {
			try {
				responseJson = new JSONObject(result.toString());
				saveInDatabase(responseJson);
				startLibraryActivity();
			    mCallback.done();
			
			} catch (Exception e) {
				Log.e("Json Exception", "Error in reading json response while registering User");
			}
		}
	}


	public static void saveInDatabase(JSONObject responseJson2) throws JSONException{
		JSONArray friends = (JSONArray) responseJson2.get("friends");
		if(friends == null )
			return ;
		SQLiteDatabase db = contextGlobal.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.OPEN_READWRITE, null);
		ContentResolver reslover = contextGlobal.getContentResolver();
		for(int i = 0 ; i < friends.length() ;i++){
		JSONObject frnd = friends.getJSONObject(i);
		JSONObject frndInfo = frnd.getJSONObject("id");
		String phoneNo = frndInfo.get("phoneNo").toString();
		String mtagId2 = frndInfo.get("mtagId").toString();
		Uri contactUri = Uri.withAppendedPath(
				PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNo));
		Cursor fromCursor = reslover.query(contactUri, new String[]{PhoneLookup.DISPLAY_NAME}, null, null, null);
		String fromName  =  null ; 
		if(fromCursor != null){
			if(fromCursor.moveToFirst()){
				fromName = fromCursor.getString(0);
			}
		}
		if(fromName == null ){
			fromName = phoneNo;
		}
	
		if(!MtagUtils.isTableExists(DatabaseConstants.CONTACT, db)){
			DsSQLLite.execute(db, DatabaseConstants.CREATE_TABLE_CONTACT);
		}
		phoneNo = MtagUtils.getNormalized(phoneNo).toString();
		String frndQuery =  "Select * from "+DatabaseConstants.CONTACT + " where "+DatabaseConstants.PHONE_NO + " = '" +phoneNo+ "'";
		Cursor cursor = DsSQLLite.rawQuery(db, frndQuery);
		if(cursor != null){
			if(!cursor.moveToFirst()){
				DsSQLLite.insert(db, DatabaseConstants.CONTACT, mtagId2,fromName,phoneNo);
			}
		}
		}
		db.close();
	}
}
