package org.mg.mtag;

import android.app.ActionBar;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.mg.util.DatabaseConstants;
import com.motlay.mtag.server.MtagUtils;

public class Select extends TabActivity{

	/** Called when the activity is first created. */
	private static  Typeface typeface= null;
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		typeface = Typeface.createFromAsset(getAssets(), "Roboto/Roboto-Light.ttf");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contacts_main);
		ActionBar ab = getActionBar();
		ab.removeAllTabs();
		ab.setLogo(R.drawable.mtag_title);
		ab.setTitle("");

		Resources res = getResources(); 
		TabHost tabHost = getTabHost(); 
		TabHost.TabSpec spec; 
		Intent currentIntent = getIntent();
		Intent friendContact ;
		friendContact = new Intent(this,FriendSelect.class);
		friendContact.putExtra("fileName", currentIntent.getStringExtra("fileName"));
		friendContact.putExtra("songName", currentIntent.getStringExtra("songName"));
		friendContact.putExtra("songSize", currentIntent.getStringExtra("songSize"));

		View tabview = createTabView(tabHost.getContext(), "MTAG Friends");

		TabSpec setContent = tabHost.newTabSpec( "MTAG Friends").setIndicator(tabview).setContent(friendContact);
		tabHost.addTab(setContent);

		Intent intent; 
		intent = new Intent().setClass(this, FullscreenActivity.class);
		intent.putExtra("fileName",currentIntent.getStringExtra("fileName"));
		intent.putExtra("songName",currentIntent.getStringExtra("songName"));
		intent.putExtra("songSize", currentIntent.getStringExtra("songSize"));


		View tabview2 = createTabView(tabHost.getContext(), "Contacts");

		TabSpec setContent2 = tabHost.newTabSpec( "Contacts").setIndicator(tabview2).setContent(intent);
		tabHost.addTab(setContent2);
		boolean tried = getIntent().getBooleanExtra("tried", false);
		if(!tried){
			SQLiteDatabase db = this.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.OPEN_READWRITE, null);
			if(!MtagUtils.isTableExists(DatabaseConstants.CONTACT, db)){
				tabHost.setCurrentTabByTag("Contacts");
			}
			db.close();
		}else{
			tabHost.setCurrentTabByTag("MTAG Friends");
		}


	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		ActionBar ab = getActionBar();
		RelativeLayout layout = (RelativeLayout) getLayoutInflater().inflate(
				R.layout.up_text, null);
		TextView upTextView = (TextView) layout.findViewById(R.id.actionbar_text);
		upTextView.setText(" Back");
		upTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
		upTextView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto/Roboto-Light.ttf"));
		upTextView.setTextColor(Color.GRAY);
		ab.setDisplayUseLogoEnabled(false);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowTitleEnabled(false);
		ab.setIcon(R.drawable.back);
		ab.setCustomView(layout);
		upTextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		 super.onCreateOptionsMenu(menu);
		
		return true;
	}
	private static View createTabView(final Context context, final String text) {
		View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(text);
		return view;
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); 
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); 
	}


	private final int MTAG_FRIENDS = 0;
	private final int CONTACTS = 1;
}