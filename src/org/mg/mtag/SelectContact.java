package org.mg.mtag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ShareActionProvider;
import android.widget.Toast;

import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;
import com.motlay.mtag.server.FileUpload;
import com.motlay.mtag.server.MtagUtils;

public class SelectContact extends Activity {

	private Cursor cursor;
	private Cursor friendCursor;
	private ListView allContactsView;
	private ShareActionProvider mShareActionProvider;
	private String phoneNumber;
	private List<String> dupilcateContacts;
	private List<Contact> filteredContacts;
	private List<Contact> contacts;
	ContactsAdapter adapter;
	ListView listView = null;
	private EditText inputSearch;
	int textLength = 0;
	private final static int REQUEST_ENABLE_BT = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_contact);
		initViews();

	}

	private void initViews() {
		inputSearch = (EditText) findViewById(R.id.searchContact);
		listView = (ListView) findViewById(R.id.contactView);
		listView.setTextFilterEnabled(true);
		contacts = new ArrayList<Contact>();
		fillContacts();
		adapter = new ContactsAdapter(getApplicationContext(),contacts ,cursor ,friendCursor);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int postion,
					long arg3) {
				sendTo(postion);
				finish();
			}
		});
		inputSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence cs, int start, int before, int count) {
				filteredContacts = new ArrayList<Contact>();
				String text = inputSearch.getText().toString();
				textLength = text.length();
				for(int i=0 ;i<contacts.size() ;i++){
					Contact contact = contacts.get(i);
					String name = contact.getName();
					if(textLength <= name.length()){
						if(text.equalsIgnoreCase(name.substring(0,textLength)))
							filteredContacts.add(contact);
					}
				}
				contacts = filteredContacts;
				listView.setAdapter(new ContactsAdapter(getApplicationContext(),filteredContacts ,cursor ,friendCursor));
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
	}

	protected void sendTo(int position) {

		Contact toUserContact = contacts.get(position);
		String toUser = toUserContact.getNumber();
		String personName =toUserContact.getName();
		SQLiteDatabase db = this.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.CREATE_IF_NECESSARY , null);
		Cursor fromUserCursor = DsSQLLite.getFieldFromFirstRow(db, DatabaseConstants.ACCOUNT, DatabaseConstants.MTAG_ID);
		fromUserCursor.moveToFirst();
		String fromUser = fromUserCursor.getString(0);
		Intent intent = getIntent();
		String songPath = intent.getStringExtra("fileName");
		String songName = intent.getStringExtra("songName");
		String songSize = intent.getStringExtra("songSize");
		this.phoneNumber = toUser; 
		Map<String , String> info = new HashMap<String, String>();
		info.put("fileName", songPath);
		info.put("songName", songName);
		info.put("firstName",personName);
		info.put("from", fromUser);
		info.put("to", toUser);
		if(MtagUtils.isInternetAvialable(this)){
			FileUpload sendSong = new FileUpload(getApplicationContext(),info);
			sendSong.execute("");
			//startService(info);
			addToUpload(db,songName, songPath, fromUser, toUser , DatabaseConstants.TRUE);
			Toast.makeText(this, "Uploading Song " + songName + "  ("+ songSize + " )" , Toast.LENGTH_LONG).show();
		}else {
			Toast.makeText(this, "Ineternet Not Avialable song added to upload queue Song Name -:" +songName, Toast.LENGTH_LONG).show();
			addToUpload(db,songName, songPath, fromUser, toUser , DatabaseConstants.FALSE);
		}
		finish();
	}


	private void addToUpload(SQLiteDatabase db,String songName , String songPath , String fromUser , String toUser ,String uploaded) {
		if(!MtagUtils.isTableExists(DatabaseConstants.SHARED, db)){
			DsSQLLite.execute(db, DatabaseConstants.CREATE_TABLE_SHARED);
		}
		DsSQLLite.insert(db, DatabaseConstants.SHARED, toUser , fromUser , songPath ,songName ,uploaded);
	}

	/**
	 * query on contact provider and return Contacts Cursor
	 * @return
	 */
	public void fillContacts(){
		dupilcateContacts =  new ArrayList<String>();
		Uri contentUri = Phone.CONTENT_URI;
		ContentResolver reslover = getContentResolver();
		Cursor allContactsCursor = reslover.query(contentUri, null,  ContactsContract.Data.HAS_PHONE_NUMBER + " >? ",
				new String[] { "0" }, Phone.DISPLAY_NAME + " ASC");
		String name = null;
		String number = null;
		String photoId= null;
		if(allContactsCursor != null){ 
			allContactsCursor.moveToFirst();
			this.cursor = allContactsCursor;
			while (!allContactsCursor.isAfterLast()){
				number = allContactsCursor.getString(allContactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				if(number == null){
					allContactsCursor.moveToNext();
					continue;
				}
				if(number.startsWith("0") || number.startsWith("+91") || number.startsWith("91")){
					if(!dupilcateContacts.contains(number)){
						dupilcateContacts.add(number);
						name = allContactsCursor.getString(allContactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
						photoId = allContactsCursor.getString(allContactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.PHOTO_ID));
						Contact contact = new Contact(photoId, name, MtagUtils.getNormalized(number).toString());
						contacts.add(contact);
						allContactsCursor.moveToNext();
					}else{
						allContactsCursor.moveToNext();
					}
				}else{
					allContactsCursor.moveToNext();
				}

			}
		}
	}


	public Cursor getFriendsContacts(){
		SQLiteDatabase db = this.openOrCreateDatabase(DatabaseConstants.MTAG, SQLiteDatabase.CREATE_IF_NECESSARY, null);
		Cursor friendsCursor = null;
		try {
			friendsCursor = DsSQLLite.get(db, DatabaseConstants.ACCOUNT, DatabaseConstants.MTAG_ID,DatabaseConstants.FIRST_NAME,DatabaseConstants.PHONE_NO);
		}catch (Exception e){
			return friendsCursor;
		}
		if(friendsCursor.getCount() == 1)
			return null;
		return friendsCursor;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dedication_screen, menu);
		// Locate MenuItem with ShareActionProvider
		MenuItem item = menu.findItem(R.id.menu_item_share);
		// Fetch and store ShareActionProvider
		mShareActionProvider = (ShareActionProvider) item.getActionProvider();
		// Return true to display menu
		return true;
	}

}
