package org.mg.mtag;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;
import com.motlay.mtag.server.FileUpload;
import com.motlay.mtag.server.MtagUtils;

public class SelectFriend extends Activity{
	private ListView listView = null;
	private Cursor friendCursor;
	@Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
		 super.onCreate(savedInstanceState);
         setContentView(R.layout.select_friends);
         intitViews();
         
    }

	private void intitViews() {
		listView = (ListView) findViewById(R.id.friendsContact);
		friendCursor = getFriendsContacts();
		FriendsContactAdapter adapter = new FriendsContactAdapter(this, friendCursor);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				sendTo(arg2);
			}
		});
	}
	
	protected void sendTo(int position) {
		friendCursor.moveToPosition(position);
		String toUser = friendCursor.getString(2);
		SQLiteDatabase db = this.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.CREATE_IF_NECESSARY , null);
		Cursor fromUserCursor = DsSQLLite.getFieldFromFirstRow(db, DatabaseConstants.ACCOUNT, DatabaseConstants.MTAG_ID);
		fromUserCursor.moveToFirst();
		String fromUser = fromUserCursor.getString(0);
		Intent intent = getIntent();
		String songPath = intent.getStringExtra("fileName");
		String songName = intent.getStringExtra("songName");
		String songSize = intent.getStringExtra("songSize");
		Map<String,String > info = new HashMap<String, String>();
		info.put("fileName", songPath);
		info.put("songName", songName);
		info.put("firstName",friendCursor.getString(1));
		info.put("from", fromUser);
		info.put("to", toUser);
		if(MtagUtils.isInternetAvialable(this)){
			addToUpload(db,songName, songPath, fromUser, toUser , DatabaseConstants.TRUE);
			FileUpload sendSong = new FileUpload(getApplicationContext(),info);
			sendSong.execute("");
		}else {
			Toast.makeText(this, "Ineternet Not Avialable song added to upload queue Song Name -:" +songName, Toast.LENGTH_LONG).show();
			addToUpload(db,songName, songPath, fromUser, toUser , DatabaseConstants.FALSE);
		}
		finish();
	}
	private void addToUpload(SQLiteDatabase db,String songName , String songPath , String fromUser , String toUser ,String uploaded) {
		if(!MtagUtils.isTableExists(DatabaseConstants.SHARED, db)){
			DsSQLLite.execute(db, DatabaseConstants.CREATE_TABLE_SHARED);
		}
		DsSQLLite.insert(db, DatabaseConstants.SHARED, toUser , fromUser , songPath ,songName ,uploaded,String.valueOf(System.currentTimeMillis()));
	}
	
	public Cursor getFriendsContacts(){
		SQLiteDatabase db = this.openOrCreateDatabase(DatabaseConstants.MTAG, SQLiteDatabase.CREATE_IF_NECESSARY, null);
		Cursor friendsCursor = null;
		try {
			friendsCursor = DsSQLLite.get(db, DatabaseConstants.CONTACT, DatabaseConstants.MTAG_ID,DatabaseConstants.FIRST_NAME,DatabaseConstants.PHONE_NO);
		}catch (Exception e){
			return friendsCursor;
		}
		return friendsCursor;
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); 
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); 
	}
}
