package org.mg.mtag;

import org.kreed.vanilla.util.SystemUiHider;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.mg.coverarts.CoverArts;
import com.mg.ds.sqllite.DsSQLLite;
import com.mg.util.DatabaseConstants;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class SplashScreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		if(!getIntent().getBooleanExtra("status",true)){
			finish();
		}
		SQLiteDatabase db = this.openOrCreateDatabase(DatabaseConstants.MTAG,SQLiteDatabase.CREATE_IF_NECESSARY, null);
		String mtagId = null;
		String verfied = "";
		try{
			String sql = "select * from account ";
			Cursor cursor = DsSQLLite.rawQuery(db, sql);
			 if(cursor != null){
					if (cursor.moveToFirst()) {
							mtagId =  cursor.getString(0);
							verfied = cursor.getString(3);
					}
			  }
		}catch(Exception e) {
			Log.d("Exception while starting MTAG", "Splash Screen");
		}
		db.close();
		LoadCoverArts coverarts = new LoadCoverArts(getContentResolver());
		coverarts.execute("");
		if(verfied.equalsIgnoreCase("false") || verfied.isEmpty()) {
		 Handler handler = new Handler();
		    handler.postDelayed(new Runnable() {
		         public void run() {
		        	 if(getIntent().getBooleanExtra("status",true))
		        		 startLogin();
		        	 
		         }
		    }, 4000);
		}else{
			 Handler handler = new Handler();
			    handler.postDelayed(new Runnable() {
			         public void run() {
			        	startLibray();
			         }
			    }, 2000);
		}
	}
	
	protected void startLibray() {
		Intent intent = new Intent(this, LibraryActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
		
	}

	protected void startLogin() {
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
		finish();
	}
	
	public static class LoadCoverArts extends  AsyncTask<Object, Object, Object>{
		private ContentResolver resolver ;
		public LoadCoverArts(ContentResolver contentResolver){
			resolver = contentResolver;
		}
		
		@Override
		protected Object doInBackground(Object... params) {
			CoverArts covers = CoverArts.getInstance();
			covers.loadCoverArts(resolver);
			return null;
		}
		
	}
	
}
