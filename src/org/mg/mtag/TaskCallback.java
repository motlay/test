package org.mg.mtag;

public interface TaskCallback {
	public void done() ;
}
